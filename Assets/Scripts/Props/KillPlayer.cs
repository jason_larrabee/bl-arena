﻿using UnityEngine;


public class KillPlayer : MonoBehaviour {

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Player")
        {
            Player_Manager pInfo = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Manager>();
            pInfo.pStats.ModifyVital(0, -pInfo.pStats.MaxHealth);
        }
    }

}
