﻿using UnityEngine;


public class TreasureChest : MonoBehaviour {
    //Basic Information
    [SerializeField]
    string tcName;
    [SerializeField]
    int size = 0;       //Sets the max number of items the chest can have MAX 10

    //Effects
    [SerializeField]
    Animator anim;
    [SerializeField]
    ParticleSystem ps;
    
    //Chest content tracking
    [SerializeField]
    GameObject[] contents;
    [SerializeField]
    int numItems;
    [SerializeField]
    GameObject[] lootTable;

    //Flags
    bool isOpen = false;
    
    void Start()
    {
        ps.Stop();
        GenerateLoot();
    }

    public void Interact()
    {
        if (!isOpen) OpenChest();
    }

    public void Empty()
    {
        CloseChest();
    }

    private void GenerateLoot()
    {
        //Determine number of items
        int lootSize = Random.Range(0, size);
        numItems = lootSize;
        for(int i = 0; i < lootSize; i++)
        {
            int lootItem = Random.Range(0, lootTable.Length);
            contents[i] = lootTable[lootItem];
        }
    }

    private void OpenChest()
    {
        anim.Play("box_open");
        ps.Play();
        isOpen = true;
    }

    private void CloseChest()
    {
        anim.Play("box_close");
        ps.Stop();
        isOpen = false;
    }

    public string Name
    {
        get
        {
            return tcName;
        }
    }

    public GameObject[] Contents
    {   
        get
        {
            return contents;
        }
    }

    public int NumItems
    {
        get
        {
            return numItems;
        }
        set
        {
            numItems = value;
        }
    }
}
