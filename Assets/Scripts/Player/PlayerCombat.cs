﻿using UnityEngine;
using System.Collections;

public class PlayerCombat : MonoBehaviour {

    //Player Information
    [SerializeField]
    private Player_Manager pInfo;
    //Spawn point for projectiles
    [SerializeField]
    private Transform projectileSpawnPoint;

    //Combat state flags
    private bool isAttacking = false;
    private bool isShooting = false;
    private bool isCasting = false;
    private bool isBlocking = false;
 
    private float attackAnimationTimer = 0.833f;
 

    //    public void SkillHitTarget(int a)
    //    {
    //        //Called when a skill is cast that hits the target
    //        ItemDetails sInfo = pInfo.pSkills.playerSkills[a].GetComponent<ItemDetails>();
    //        //Difference between total potential damage and enemies defense level
    //        int d = (pInfo.pStats.modMagicAttack + (sInfo.itemStat * pInfo.pXP.skillLevel[a]) + (pInfo.pStats.modIntelligence / 10)) - eInfo.eStats.modMagicDefense;
    //        //if positive, pull random number in range of 0 and difference for actual damage
    //        if (d > 0)
    //        {
    //            int damage = Random.Range(0, d);
    //            //Remove the health
    //            eInfo.eVitals.Health -= damage;
    //            //Give hit XP
    //            pInfo.pXP.GiveSkillXP(a, d / 10);

    //        }
    //    }

    //    public void SkillDoTHitTarget(int a)
    //    {
    //        //Called when a skill is cast that hits the target
    //        ItemDetails sInfo = pInfo.pSkills.playerSkills[a].GetComponent<ItemDetails>();
    //        //Difference between total potential damage and enemies defense level
    //        int d = (pInfo.pStats.modMagicAttack + (sInfo.itemStat * pInfo.pXP.skillLevel[a]) + (pInfo.pStats.modIntelligence / 10)) - eInfo.eStats.modMagicDefense;
    //        //if positive, pull random number in range of 0 and difference for actual damage
    //        if (d > 0)
    //        {
    //            int damage = Random.Range(0, d);
    //            //Set the dot
    //            eInfo.eVitals.DamageOverTime(damage, 5);
    //        }
    //    }


    //**********************************************************************************************
    // BASIC ATTACK METHODS
    //  Called by PlayerAttack() in the player manager when the player presses the attack button
    //
    // public void Attack() - Melee
    // public void Shoot()  - Ranged
    // public void Cast()   - Magic
    //
    //  Validates the call and ensures all requirements are met.
    //  Removes the necessary vital cost
    //  Sets appropriate flags
    //  Starts appropriate coroutines to fire events
    //  Grants basic experience
    //**********************************************************************************************
    // void Attack()
    public void Attack()
    {
        //ERROR CHECKING
        //Make sure we are in combat mode
        if (!pInfo.InCombat) return;

        //Make sure we aren't already attacking
        if (isAttacking || isShooting || isCasting || isBlocking) return;

        //Enough stamina?
        if (pInfo.pStats.Stamina < 5.0f) return;
        else
        {
            //Take stamina
            pInfo.pStats.ModifyVital(2, -5.0f);
        }

        //Flag attacking for animator start the ExitAttacking() coroutine to cancel the isAttacking flag
        isAttacking = true;
        

        if (pInfo.pInv.Equipped[1] != -1)
        {
            //If there is a weapon equipped, set its damage flag and maximum potential damage
            ItemDetails iInfo = pInfo.pInv.Inventory[pInfo.pInv.Equipped[1]].GetComponent<ItemDetails>();
            iInfo.CanDamage = true;
            iInfo.MaxDamage = pInfo.pStats.MeleeAttack;
        }
        StartCoroutine(ExitAttacking());

        //Give player attempt experience
        //Strength
        pInfo.pXP.GiveXP(0, 3, 1.0f);
        //Dexterety
        pInfo.pXP.GiveXP(0, 4, 1.0f);
        //Melee Attack
        pInfo.pXP.GiveXP(0, 6, 1.0f);
    }
    // END: Attack()
    // void Shoot()
    public void Shoot()
    {
        //ERROR CHECKING
        //Make sure we are in combat mode
        if (!pInfo.InCombat) return;

        //Make sure we aren't already attacking
        if (isAttacking || isShooting || isCasting || isBlocking ) return;

        //Enough stamina?
        if (pInfo.pStats.Stamina < 5.0f) return;
        else
        {
            //Take stamina
            pInfo.pStats.ModifyVital(2, -5.0f);
        }

        //Flag attacking for animator start the ExitAttacking() coroutine to cancel the isAttacking flag
        isShooting = true;
        //If there is a weapon equipped, set its damage flag
        ItemDetails iInfo = pInfo.pInv.Inventory[pInfo.pInv.Equipped[1]].GetComponent<ItemDetails>();
        StartCoroutine(ExitShooting());

        //Give player attempt experience
        //Dexterety
        pInfo.pXP.GiveXP(0, 4, 1.0f);
        //Strength
        pInfo.pXP.GiveXP(0, 3, 1.0f);
        //Ranged attack
        pInfo.pXP.GiveXP(0, 7, 1.0f);
    }
    // END: Shoot()
    // void Cast()
    public void Cast()
    {
        //ERROR CHECKING
        //Make sure we are in combat mode
        if (!pInfo.InCombat) return;

        //Make sure we aren't already attacking
        if (isAttacking || isShooting || isCasting || isBlocking) return;

        //Enough Mana?
        if (pInfo.pStats.Mana < 5.0f) return;
        //If so, take the mana
        else
        {
            //Take mana
            pInfo.pStats.ModifyVital(4, -5.0f);
        }

        //Flag casting for animator start the ExitCasting() coroutine to cancel the isCasting flag
        isCasting = true;
        StartCoroutine(ExitCasting());
        StartCoroutine(SpawnProjectile(pInfo.pInv.Inventory[pInfo.pInv.Equipped[1]].GetComponent<ItemDetails>().Projectile));

        //Give Experience
        //Intelligence
        pInfo.pXP.GiveXP(0, 5, 1.0f);
        //Magic Attack
        pInfo.pXP.GiveXP(0, 8, 1.0f);
    }
    // END: Cast()
    // END: BASIC ATTACK METHODS *******************************************************************


    //**********************************************************************************************
    // void Defend()
    //
    // Called when the player pressed the defend button.  If the player is defending when hit, he 
    //  will take reduced damage
    //**********************************************************************************************
    public void Defend()
    {
        //ERROR CHECKING
        //Make sure we are in combat mode
        if (!pInfo.InCombat) return;

        //Make sure we aren't already attacking
        if (isAttacking || isShooting || isCasting || isBlocking) return;

        //Enough stamina?
        if (pInfo.pStats.Stamina < 5.0f) return;
        else
        {
            //Take stamina
            pInfo.pStats.ModifyVital(2, -5.0f);
        }

        isBlocking = true;

        StartCoroutine(ExitDefending());

        //Give player attempt experience
        //Strength
        pInfo.pXP.GiveXP(0, 3, 1.0f);
        //Dexterety
        pInfo.pXP.GiveXP(0, 4, 1.0f);
        //Physical Defense
        pInfo.pXP.GiveXP(0, 9, 1.0f);
    }
    // END: void Defend() **************************************************************************

    //*******************************************************************
    // IEnumerator SpawnProjectile(Rigidbody proj)
    //  Rigidbody proj: Rigid body to spawn
    //
    // Waits for animation, then spawns the projectile at the projectile 
    //  spawn point
    //*******************************************************************
    IEnumerator SpawnProjectile(Rigidbody proj)
    {
        //Wait for animation
        yield return new WaitForSeconds(0.5f);
        //Spawn the projectile
        Rigidbody projectileSpawned = (Rigidbody)Instantiate(proj, projectileSpawnPoint.position, projectileSpawnPoint.rotation);
        projectileSpawned.AddRelativeForce(Vector3.forward * 2000);
     }
    // END: IEnumerator SpawnProjectile() *******************************

    //*******************************************************************
    // IEnumerator ExitAttacking()
    //
    // Waits attack time and exits isAttacking state
    //*******************************************************************
    IEnumerator ExitAttacking()
    {
        yield return new WaitForSeconds(attackAnimationTimer);
        isAttacking = false;
        //Reset canDamage on equipped weapon
        if (pInfo.pInv.Equipped[1] != -1)
        {
            pInfo.pInv.Inventory[pInfo.pInv.Equipped[1]].GetComponent<ItemDetails>().CanDamage = false;
        }
    }
    // END: IEnumerator ExitAttacking() *********************************

    //*******************************************************************
    // IEnumerator ExitShooting()
    //
    // Waits for animation and exits isShooting state
    //*******************************************************************
    IEnumerator ExitShooting()
    {
        yield return new WaitForSeconds(0.5f);
        isShooting = false;
    }
    // END: IEnumerator ExitShooting() *********************************

    //*******************************************************************
    // IEnumerator ExitCasting()
    //
    // Waits attack time and exits isAttacking state
    //*******************************************************************
    IEnumerator ExitCasting()
    {
        yield return new WaitForSeconds(attackAnimationTimer);
        isCasting = false;
    }
    // END: IEnumerator ExitCasting() *********************************

    //*******************************************************************
    // IEnumerator ExitDefending()
    //
    // Waits attack time and exits isDefending state
    //*******************************************************************
    IEnumerator ExitDefending()
    {
        yield return new WaitForSeconds(0.333f);
        isBlocking = false;
    }
    // END: IEnumerator ExitCasting() *********************************

    //*************************************************************************
    // ACCESSORS
    //*************************************************************************
    // - isAttacking
    public bool IsAttacking
    {
        get
        {
            return isAttacking;
        }
    }
    // - isShooting
    public bool IsShooting
    {
        get
        {
            return isShooting;
        }
    }
    // - isCasting
    public bool IsCasting
    {
        get
        {
            return isCasting;
        }
    }
    // - isBlocking
    public bool IsBlocking
    {
        get
        {
            return isBlocking;
        }
    }
}
