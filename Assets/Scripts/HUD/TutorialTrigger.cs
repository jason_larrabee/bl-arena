﻿using UnityEngine;

public class TutorialTrigger : MonoBehaviour {
    //***************************************************************************
    // TutorialTrigger.cs
    //
    // Trigger script to launch the corresponding tutorial
    //***************************************************************************
    [SerializeField]
    private TutorialManager tMan;       //The tutorial manager

    //***************************************************************************
    // void OnTriggerEnter(Collider other)
    //  Collider other: collider entering the trigger
    //
    // Check to see if other is the player, if so it shows the current tutorial
    //***************************************************************************
    public void OnTriggerEnter(Collider other)
    {
        //Make sure it is a the player
        if (other.tag == "Player")
        {
            //Get the player reference
            Player_Manager pInfo = other.GetComponent<Player_Manager>();
            //Give the tutorial call
            tMan.ShowTutorial(pInfo.TutorialLevel);
            //Increase the players tutorial level
            pInfo.TutorialLevel++;
            //Disable this trigger so it doesn't re-activate if the player passes back through
            this.gameObject.SetActive(false);
        }
    }
    // END : void OnTriggerEnter(Collider other) ********************************
}
