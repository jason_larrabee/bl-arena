﻿using UnityEngine;

public class Lift_Trigger : MonoBehaviour {

    public Terrain terrain;

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            if (terrain.gameObject.activeSelf)
            {
                terrain.gameObject.SetActive(false);
            }
            else
            {
                terrain.gameObject.SetActive(true);
            }
        }
    }
}
