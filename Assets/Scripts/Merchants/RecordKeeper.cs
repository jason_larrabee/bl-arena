﻿using UnityEngine;

public class RecordKeeper : MonoBehaviour {
    //**********************************************************************************************
    //  RecordKeeper.cs
    //
    //  Tracks the players progress through the arenas.  Allows them to move on to the next arena
    //  when they have reached the next arenas win requirements.
    //
    //  FUTURE:  Allows the player to save their progress and resume at another time
    //
    //**********************************************************************************************

    //Number of wins needed for next arena
    public int winsNeeded;
     //Dialogue strings
    public string[] dialogue;

    //*********************************************************************
    // void Start()
    //
    // Initializes the dialogue strings
    //*********************************************************************
	void Start ()
    {
        dialogue = new string[2];
        dialogue[0] = "You do not have enough wins for the next level.  Please come back after you have aquired " + winsNeeded + " victories.";
        dialogue[1] = "Congratulations! It seems you are ready to move on to the next arena. Would you like to go now?";
	}
    // END: void Start() *************************************************
	


}
