﻿using UnityEngine;

public class PlayerInput : MonoBehaviour {
    //****************************************************************************************
    // PlayerInput.cs
    //
    // Listens for and handles all player input
    //****************************************************************************************
    [SerializeField]
    private Player_Manager pInfo;                               //The player manager
    public Camera mainCam;                                      //Main camera

    //Hotkeys system
    public int[] skillHotkeys;                      
    public int skillSetHotkey;

    //Misc vars
    public float selectedDistance;		                        // Distance to selected object
    private const float MAX_INTERACT_DISTANCE = 4.0f;		    // Maximum distance from object to be able to pick it up

    //***********************************************************************
    // void Start()
    //
    // Initializes script and sets references
    //***********************************************************************
    void Start ()
    {
        //Get our player information 
    	pInfo = GetComponent<Player_Manager>();

     	// no object yet so 0
    	selectedDistance = 0.0f;
    }
    // END: void Start() ****************************************************

    //***********************************************************************
    // void Update()
    //
    // Listens for player input and freezes out the controls if necessary
    //***********************************************************************
    void Update()
    {
        //Freeze player if necessary
        CheckFreezePlayerControls();

        //Player Input Checks              
        //HUD PANELS//
        //Character Panel
        if (Input.GetKeyUp("p"))
        {
            pInfo.gMan.ToggleCharPanel();
        }
        //Inventory Panel
        if (Input.GetKeyUp("i"))
        {
            pInfo.gMan.ToggleInventoryPanel();
        }
        //Skill panel
        if (Input.GetKeyUp("k"))
        {
            pInfo.gMan.ToggleSkillPanel();
        }

        //Exit menu
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            pInfo.gMan.ToggleExitGamePanel();
        }

        //Interact
        if (Input.GetKeyUp("r"))
        {
            Interact();
        }

        //HOTKEYS//
        //1
        if (Input.GetKeyUp("1"))
        {
            pInfo.gMan.UseHotkey(0);
        }
        //2
        if (Input.GetKeyUp("2"))
        {
            pInfo.gMan.UseHotkey(1);
        }
        //3
        if (Input.GetKeyUp("3"))
        {
            pInfo.gMan.UseHotkey(2);
        }
        //4
        if (Input.GetKeyUp("4"))
        {
            pInfo.gMan.UseHotkey(3);
        }
        //5
        if (Input.GetKeyUp("5"))
        {
            pInfo.gMan.UseHotkey(4);
        }
        //6
        if (Input.GetKeyUp("6"))
        {
            pInfo.gMan.UseHotkey(5);
        }
        //7
        if (Input.GetKeyUp("7"))
        {
            pInfo.gMan.UseHotkey(6);
        }
        //8
        if (Input.GetKeyUp("8"))
        {
            pInfo.gMan.UseHotkey(7);
        }
        //9
        if (Input.GetKeyUp("9"))
        {
            pInfo.gMan.UseHotkey(8);
        }
        //10
        if (Input.GetKeyUp("0"))
        {
            pInfo.gMan.UseHotkey(9);
        }

        //COMBAT//
        //Attack
        if (Input.GetButton("Fire1"))
        {
            if (!pInfo.pCom.IsAttacking && !pInfo.pCom.IsCasting && !pInfo.pCom.IsShooting && !pInfo.pCom.IsBlocking)
            {
                pInfo.PlayerAttack();
            }
        }
        //Defend
        if (Input.GetButton("Fire2"))
        {
            if (!pInfo.pCom.IsAttacking && !pInfo.pCom.IsCasting && !pInfo.pCom.IsShooting && !pInfo.pCom.IsBlocking)
            {
                pInfo.pCom.Defend();
            }
        }
    }
    // END: void Update() ***************************************************

    //***********************************************************************
    // void Interact()
    //
    // Called when the user presses the interaction key
    //***********************************************************************
    private void Interact()
    {
        ////Make sure there is a valid interaction target
        if (pInfo.gMan.crosshairs != null && pInfo.gMan.crosshairs.Distance <= MAX_INTERACT_DISTANCE)
        {
            // WORLD OBJECTS
            if (pInfo.gMan.crosshairs.Target.tag == "Door")
            {
                pInfo.gMan.crosshairs.Target.GetComponentInParent<LobbyDoors>().Interact();
            }
            else if (pInfo.gMan.crosshairs.Target.tag == "Lever")
            {
                pInfo.gMan.crosshairs.Target.GetComponentInParent<Lift>().PullLever();
            }
            else if (pInfo.gMan.crosshairs.Target.tag == "OpenableContainer")
            {
                pInfo.gMan.crosshairs.Target.GetComponentInParent<TreasureChest>().Interact();
                pInfo.gMan.ToggleContainerPanel();
            }

            // NPC INTERACTIONS
            //TUTORIAL CHECKS - Give appropriate responses if the player hasnt reached this point in the tutorial
            if (pInfo.InTutorial)

            {
                if (pInfo.gMan.crosshairs.Target.name == "Mystic")
                {
                    if (pInfo.TutorialLevel < 6)
                    {
                        //pInfo.pHUD.ShowMessage("You must complete more of the tutorial to purchase from this merchant.");
                        return;
                    }
                    else
                    {
                        pInfo.gMan.ToggleMerchantPanel();
                    }
                }
                else if (pInfo.gMan.crosshairs.Target.name == "Blacksmith")
                {
                    if (pInfo.TutorialLevel < 4)
                    {
                        //pInfo.pHUD.ShowMessage("You must complete more of the tutorial to purchase from this merchant again.");
                        return;
                    }
                    else
                    {
                        if (pInfo.TutorialLevel == 4) pInfo.TutorialLevel = 5;
                        pInfo.gMan.ToggleMerchantPanel();
                    }
                }
                else if (pInfo.gMan.crosshairs.Target.name == "Arena Guard")
                {
                    if (pInfo.TutorialLevel < 5)
                    {
                        //pInfo.pHUD.ShowMessage("You must complete more of the tutorial to enter the arena. HINT: Equip a shortsword.");
                        return;
                    }
                    else if (pInfo.TutorialLevel > 5 && pInfo.pSkills.Skills[0] == null)
                    {
                        //pInfo.pHUD.ShowMessage("You must learn the skill FIREBALL before entering the arena again.");
                        return;
                    }
                    else
                    {
                        if (!pInfo.InArena)
                        {
                            pInfo.gMan.ToggleGuardPanel();
                            //Show current tutorial
                            if (pInfo.TutorialLevel == 5)
                            {
                                pInfo.gMan.tMan.ShowTutorial(pInfo.TutorialLevel);
                            }
                            
                        }
                        if (pInfo.InArena)
                        {
                            //Only allow us out if enemy is dead
                            if (!pInfo.EnemyDead)
                            {
                                //Send message to player
                                // ("You must kill or be killed in order to leave the arena.");
                            }
                            else pInfo.gMan.ToggleExitGuardPanel();
                        }
                    }
                }
            }
            //NOT TUTORIAL - Normal interactions
            else
            { 
                //Merchant interaction
                if (pInfo.gMan.crosshairs.Target.tag == "Merchant")
                {
                    pInfo.gMan.ToggleMerchantPanel();
                }
                // The target is a guard
                else if (pInfo.gMan.crosshairs.Target.name == "Arena Guard")
                {
                    if (!pInfo.InArena)
                    {
                        pInfo.gMan.ToggleGuardPanel();
                    }
                    if (pInfo.InArena)
                    {
                        //Only allow us out if enemy is dead
                        if (!pInfo.EnemyDead)
                        {
                            //Send message to player
                            // ("You must kill or be killed in order to leave the arena.");
                        }
                        else pInfo.gMan.ToggleExitGuardPanel();
                    }
                }
                else if (pInfo.gMan.crosshairs.Target.tag == "RecordKeeper")
                {
                    pInfo.gMan.ToggleRecordKeeperPanel();
                }
            }
        }
    }
    // END: void Interact()

    //***********************************************************************
    // void CheckFreezePlayerControls()
    //
    // Checks for any conditions that would lock out player interaction and
    //  locks/unlocks them accordingly
    //***********************************************************************
    private void CheckFreezePlayerControls()
    {
        //Checks if we have any UI windows open, if so lock the players movement and rotation
        if (pInfo.gMan.merchantPanel.activeSelf || pInfo.gMan.characterPanel.activeSelf || pInfo.gMan.inventoryPanel.activeSelf || pInfo.gMan.guardPanel.activeSelf || pInfo.gMan.exitGuardPanel.activeSelf || pInfo.gMan.tutorialPanel.activeSelf || pInfo.gMan.Skills.activeSelf || pInfo.gMan.deathPanel.activeSelf || pInfo.gMan.exitGamePanel.activeSelf || pInfo.gMan.rkPanel[0].activeSelf || pInfo.gMan.ContainerPanel.activeSelf)
        {
           // pInfo.gMan.MouseEnable();
            pInfo.pMC.enabled = false;
            pInfo.pMLookHoriz.enabled = false;
            pInfo.pMLookVert.enabled = false;
        }
        else
        {
         //   pInfo.pHUD.MouseDisable();
            pInfo.pMC.enabled = true;
            pInfo.pMLookHoriz.enabled = true;
            pInfo.pMLookVert.enabled = true;
        }
    }
    // END: CheckFreezePlayerControls()

}
