﻿using UnityEngine;


public class LobbyNav : MonoBehaviour {
    //************************************************************************************
    // LobbyNav.cs
    //
    // Handles the npcs that hang out in the lobbies.
    // Attach to NPC and set the waypoints from the WaypointManager waypoints in the lobby
    // Set the NPCs own Nav Mesh Agent and Animator to the Lobby Navs respective Nav and Anim slots
    // Set the update time to anything > 10.  Each time it updates with a new waypoint, a new update time is
    //  generated randomly that is +/-5 from the default setting
    //************************************************************************************
    [SerializeField]
    private string npcName;                         //Name of the NPC
    [SerializeField]
    private NavMeshAgent nav;                       //NavMeshAgent of the NPC
    [SerializeField]
    private Animator anim;                          //Animator for the NPC
    private GameObject[] waypoints;                  //List of all valid waypoints for NPC
    private GameObject currWaypoint;                 //Currently active waypoint
    private GameObject doorWaypoint;                 //Waypoint of the exit door
    [SerializeField]
    private float updateTime;                       //How often to generate a new waypoint
    private float updateTimer;                      //Time since last update

    private LobbySpawnManager lsm;                  //Lobby Spawn manager reference



	// Use this for initialization
	void Start ()
    {
        //Find the spawn manager
        lsm = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<LobbySpawnManager>();
        //Load all available waypoints
        InitWaypoints();
        //Get our first waypoint
        NewWaypoint();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Update the timer
        updateTimer += Time.deltaTime;

        //If we have reached the time limit, generate a new waypoint
        if (updateTimer >= updateTime)
        {
            NewWaypoint();
        }

        //If we have reached our desitination, stop moving and stop moving animation
        Vector3 dist = currWaypoint.transform.position - nav.transform.position;
        if(Mathf.Abs(dist.x) < 0.5f && Mathf.Abs(dist.z) < 0.5f)
        {
            anim.SetBool("isMoving", false);
            //Check if the object is at the arena doors
            if (currWaypoint == doorWaypoint)
            {
                lsm.CurNPCCount -= 1;
                //Destroy the object(it goes into the arena)
                Destroy(this.gameObject);
            }

        }
	}

    //****************************************************************
    // void InitWaypoints()
    //
    // Loads all available waypoints into the waypoints array
    // Sets the door waypoint.
    //****************************************************************
    private void InitWaypoints()
    {
        waypoints = GameObject.FindGameObjectsWithTag("Waypoint");

        //Set the door waypoint
        for (int i = 0; i < waypoints.Length; i++)
        {
            //Cycle through the list until the door is found and set it
            if (waypoints[i].name == "Waypoint_LobbyDoor")
            {
                doorWaypoint = waypoints[i];
                //Get  out of the loop
                i = waypoints.Length;
            }
        }
    }
    // END: void InitWaypoints()

    //****************************************************************
    // void NewWaypoint()
    //
    // Selects a new waypoint from the list, then sets it as the new
    //  destination.  Resets the updateTimer to prepare for next update
    //****************************************************************
    private void NewWaypoint()
    {
        //Grab waypoint
        int wp = Random.Range(0, waypoints.Length);
        currWaypoint = waypoints[wp];

        //Set destination
        nav.SetDestination(currWaypoint.transform.position);
        anim.SetBool("isMoving", true);

        //Set updateTimer
        NextUpdate();
        updateTimer = 0.0f;
    }
    // END: void NewWaypoint() ***************************************

    //****************************************************************
    // void NextUpdate()
    //
    // Calculates the next update time
    // Maintains a range of 10.0f to 30.0f
    //****************************************************************
    private void NextUpdate()
    {
        //Determine next update time +/-5.0f of current updateTime
        updateTime = Random.Range(updateTime - 5.0f, updateTime + 5.0f);

        //Keep the timer within 10 to 30 seconds
        if (updateTime < 10.0f)
        {
            updateTime = 20.0f;
        }
        else if (updateTime > 30.0f)
        {
            updateTime = 15.0f;
        }
    }
    // END: void NextUpdate() ****************************************

    //****************************************************************
    // ACCESSORS
    //****************************************************************
    // - string npcName
    public string NPCName
    {
        get
        {
            return npcName;
        }
    }
}
