﻿using UnityEngine;

public class LobbyDoors : MonoBehaviour {

    //Angle to open the door to
    private const float DOOR_OPEN_ANGLE = 240.0f;
    //Default settings
    Vector3 defaultRot;
    private Vector3 openRot;

    //Sound to play when opening the door
    [SerializeField]
    private AudioSource audio_Door;
    [SerializeField]
    private DoorLock doorLock;
    //Position/action flags
    private bool isOpen;
    private bool open;
    private bool close;

    private GUIManager gMan;

    void Start()
    {
        defaultRot = transform.localRotation.eulerAngles;
        openRot = new Vector3(defaultRot.x, defaultRot.y + DOOR_OPEN_ANGLE, defaultRot.z);

        gMan = GameObject.FindGameObjectWithTag("GUIManager").GetComponent<GUIManager>();
    }

    //******************************************************************************
    // void Update()
    //
    // Called once per frame
    // Checks if the user is using the door, if so opens/closes it
    //******************************************************************************
    void Update()
    {
        if (open)
        {
            //Open door
            transform.Rotate(-openRot * 0.005f);
            //Stop the rotation and set isOpen flag
            float curDif = openRot.y - transform.localRotation.eulerAngles.y;
            if (curDif >= 0.5f)
            {
                open = false;
                isOpen = true;
            }

        }
        else if (close)
        {
            //Close door
            transform.Rotate(openRot * 0.005f, Space.Self);

            //Stop the rotation and clear isOpen flag
            if (transform.localRotation.eulerAngles.y <= 0.5f)
            {
                transform.Rotate(defaultRot);
                isOpen = false;
                close = false;
            }
        }
    } 
    // END: void Update() **********************************************************

    //******************************************************************************
    // void Interact()
    //
    // Opens or closes the door depending on its current state.
    // Plays door audio
    //******************************************************************************
    public void Interact()
    {
        if (!doorLock.Locked)
        {
            if (isOpen)
            {
                close = true;
                audio_Door.Play();
            }
            else
            {
                open = true;
                audio_Door.Play();
            }
        }
        else
        {
            gMan.ShowMessage("Door is locked.");
        }
    }
    // END: void Interact() ********************************************************
 
}
