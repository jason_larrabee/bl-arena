﻿using UnityEngine;
using UnityEngine.UI;

public class ScrollButton : MonoBehaviour {
    //**************************************************************************************
    // ScrollButton.cs
    //
    // Place on a button to enable control over a scrollbar.
    // Set the function to Scroll and up to true to increase the slider, and false to
    //  decrease it
    //**************************************************************************************

    [SerializeField]
    private Scrollbar sBar;         // The scrollbar to control

    //**************************************************************************************
    // void Scroll(bool up)
    //  bool up: Set true to increase, false to decrease
    //
    // Scrolls up/down the scrollbar
    //**************************************************************************************
    public void Scroll(bool up)
    {
        if (up)
        {
            //Move up
            sBar.value += 0.1f;
        }
        else
        {
            //Move down
            sBar.value -= 0.1f;
        }
    }
    // END: void Scroll(bool up) ***********************************************************
}
