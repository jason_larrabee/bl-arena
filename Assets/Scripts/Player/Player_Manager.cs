﻿using UnityEngine;


public class Player_Manager : MonoBehaviour {
    //******************************************************************************************************
    // Player_Manager.cs
    //
    // The main player management script.
    // Ties all other player scripts together into one access point
    // Handles all whole player effects and methods
    //******************************************************************************************************

    //Script references
    public Player_Stats pStats;
    public Player_Skills pSkills;
    public Player_Experience pXP;
    public Player_Inventory pInv;
    public PlayerInput pInput;
    public PlayerCombat pCom;
    public PlayerAnimation pAnim;
    public PlayerMovementController pMC;
    public MouseLook pMLookHoriz;
    public MouseLook pMLookVert;
    //GUI Manager
    public GUIManager gMan;

    //Spawn manager
    [SerializeField]
    private SpawnManager spawnManager;

    //Character creation
    private int archetype = 0;
        // - 0: Melee
        // - 1: Ranged
        // - 2: Magic
        // - 3: Custom
        [SerializeField]
    private int tutorialLevel = 0;

    //Equipment rendering
    [SerializeField]
    private GameObject[] equipmentRender;

    //Flags
    private bool isNewGame = true;
    private bool inTutorial = true;
    private bool inCombat = false;
    // - Arena flags
    private bool inArena = false;
    private bool enemyDead = false;

    //******************************************************************************************
    // void Start()
    //
    // Initializes the player manager.
    // Gets all player script references.
    //******************************************************************************************
    void Start () {
        //Disable all equipment
        if (equipmentRender.Length > 0)
        {
            for (int i = 0; i < equipmentRender.Length; i++)
            {
                equipmentRender[i].SetActive(false);
            }
        }

	}
    // END: void Start() ***********************************************************************
	
    //******************************************************************************************
    // public void PlayerAttack()
    //
    // Determines the equipped weapon type and calls the correct attacking method
    //******************************************************************************************
    public void PlayerAttack()
    {
         //Check equipped
        if (pInv.Equipped[1] == -1) return;

        ItemDetails iInfo = pInv.Inventory[pInv.Equipped[1]].GetComponent<ItemDetails>();
        if (iInfo.WeaponType == 0)
            pCom.Attack();
        else if (iInfo.WeaponType == 1)
            pCom.Shoot();
        else if (iInfo.WeaponType == 2)
            pCom.Cast();
    }
    // END: void PlayerAttack() ****************************************************************
    //******************************************************************************************
    // private void PlayerDie()
    //
    // Kills the player and returns them to the lobby
    //******************************************************************************************
    private void PlayerDie()
    {
        //Return the players vital stats to maximum
        pStats.ModifyVital(0, pStats.MaxHealth);
        pStats.ModifyVital(2, pStats.MaxStamina);
        pStats.ModifyVital(4, pStats.MaxMana);
        
        //Return the player to the proper respawn
        //spawnManager.SpawnPlayer(true);

    }
    // END: void PlayerDie() *******************************************************************

    //******************************************************************************************
    // public void KillPlayer()
    //
    // CALLED BY: GUI MANAGER -> Death panel -> Respawn button
    // Kills the player and calls the PlayerDie() method
    //******************************************************************************************
    public void KillPlayer()
    {    
        //Add a loss ONLY if fighting an arena fight
        if (inArena)
            pStats.Losses += 1;

        PlayerDie();
    }
    // END: void KillPlayer() ******************************************************************

    //******************************************************************************************
    // void ActivateEquipped(string eqName)
    //   string eqName: Name of the equipped item to activate
    //
    // Enables the equipped item for display
    //******************************************************************************************
    public void ActivateEquipped(string eqName)
    {
        //Error checking: Only activate if there are items in the array
        if (equipmentRender.Length <= 0)
        {
            Debug.LogError("Error: There are no items in the equipmentRender array.");
            return;
        }
        //Error checking: Blank name passed
        else if (eqName == null)
        {
            Debug.LogError("Error: No item name passed.");
            return;
        }
        //Search the array for the item requested
        else
        {
            for(int i = 0; i < equipmentRender.Length; i++)
            {
                ItemDetails iInfo = equipmentRender[i].GetComponent<ItemDetails>();
                //Item found, activate it
                if (iInfo.Name == eqName)
                {
                    iInfo.EquippedInvNumber = pInv.Equipped[1];
                    equipmentRender[i].SetActive(true);
                    return;
                }
                //We should only get here if there is no item found so give an error
                if (i == equipmentRender.Length - 1)
                {
                    Debug.LogError("Error: Requested item not found.");
                    return;
                }
            }
        }
    }
    // END: void ActivateEquipped(string eqName) ***********************************************

    //******************************************************************************************
    // void DectivateEquipped(string eqName)
    //   string eqName: Name of the equipped item to deactivate
    //
    // Disables the unequipped item for display
    //******************************************************************************************
    public void DeactivateEquipped(string eqName)
    {
        //Error checking: Only activate if there are items in the array
        if (equipmentRender.Length <= 0)
        {
            Debug.LogError("Error: There are no items in the equipmentRender array.");
            return;
        }
        //Error checking: Blank name passed
        else if (eqName == null)
        {
            Debug.LogError("Error: No item name passed.");
            return;
        }
        //Search the array for the item requested
        else
        {
            for (int i = 0; i < equipmentRender.Length; i++)
            {
                ItemDetails iInfo = equipmentRender[i].GetComponent<ItemDetails>();
                //Item found, activate it
                if (iInfo.Name == eqName)
                {
                    iInfo.EquippedInvNumber = -1;
                    equipmentRender[i].SetActive(false);
                    return;
                }
                //We should only get here if there is no item found so give an error
                if (i == equipmentRender.Length - 1)
                {
                    Debug.LogError("Error: Requested item not found.");
                    return;
                }
            }
        }
    }
    // END: void DectivateEquipped(string eqName) **********************************************

    //******************************************************************************************
    // void EnterArena()
    //
    // Sets the player up for combat in the arena
    //******************************************************************************************
    public void EnterArena()
    {
        gMan.ToggleGuardPanel();
        //Flag them as in the arena
        inArena = true;
        //Make sure enemy is not flagged dead
        enemyDead = false;

        //Put the player into combat mode
        EnterCombatMode();

    }
    // END: void EnterArena() ******************************************************************

    //******************************************************************************************
    // void ExitArena()
    //
    // Takes the player out of arena combat mode
    //******************************************************************************************
    public void ExitArena()
    {
        gMan.ToggleExitGuardPanel();
        //Flag them as in the arena
        inArena = false;
 
        //Put the player into combat mode
        ExitCombatMode();
        //TUTORIAL CHECK 
        if (inTutorial)
        {
            if (tutorialLevel == 5)
            {
                tutorialLevel = 6;
                gMan.tMan.ShowTutorial(tutorialLevel);
            }
        }
    }
    // END: void ExitArena() *******************************************************************

    //******************************************************************************************
    // void EnterCombatMode()
    //
    // Sets inCombat to true so the player may enter combat
    //******************************************************************************************
    public void EnterCombatMode()
    {
        inCombat = true;
    }
    // END: void EnterCombatMode() *************************************************************

    //******************************************************************************************
    // void ExitCombatMode()
    //
    // Removes the player from combat mode, preventing them from using attacks or skills
    //******************************************************************************************
    public void ExitCombatMode()
    {
        inCombat = false;
    }
    // END: void ExitCombatMode() **************************************************************

    //******************************************************************************************
    // void GiveCombatExperience(int attackType, float amount)
    //
    // Gives the requested amount of experience to the appropriate skill
    //******************************************************************************************
    public void GiveCombatExperience(int attackType, float amount)
    {
        pXP.GiveXP(0, attackType + 6, amount);
    }
    // END: void GiveCombatExperience(int attackType, float amount) ****************************

    //*******************************************************************************************
    // void ExitGame()
    //
    // Exits the game and quits the application
    //*******************************************************************************************
    public void ExitGame()
    {
        Application.Quit();
    }
    // END: void ExitGame() *********************************************************************

    //*************************************************************************************
    // void EndTutorial()
    //
    // Ends the games tutorial and deposits the player into the lobby of the first level.
    // Grants the player an additional 150 Gold to purchase equipment with
    //*************************************************************************************
    public void EndTutorial()
    {
        //Give 150 gold starting cash
        pInv.Gold += 100;
        gMan.UpdateInventoryPanel();

        //Set tutorial level to -1 
        tutorialLevel = -1;
        inTutorial = false;

        //Exit tutorial zone
        spawnManager.TeleportPlayer(0);
    }
    // END: void SkipTutorial() ***********************************************************

    //*************************************************************************************
    // void SkipTutorial()
    //
    // Skips the games tutorial and deposits the player into the lobby of the first level.
    // Grants the player an additional 150 Gold to purchase equipment with
    //*************************************************************************************
    public void SkipTutorial()
    {
        //Hide the tutorial panel
        gMan.ToggleTutorialPanel();

        //Give 150 gold starting cash, player will have 200 when beginning level one
        pInv.Gold += 150;
        gMan.UpdateInventoryPanel();

        //Set tutorial level to -1 
        tutorialLevel = -1;
        inTutorial = false;

        //Exit tutorial zone
        spawnManager.TeleportPlayer(0);
    }
    // END: void SkipTutorial() ***********************************************************

    //******************************************************************************************
    // PROPERTIES
    //******************************************************************************************
    public bool IsNewGame
    {
        get
        {
            return isNewGame;
        }
    }
    public bool InTutorial
    {
        get
        {
            return inTutorial;
        }
        set
        {
            inTutorial = value;
        }
    }
    public int Archetype
    {
        get
        {
            return archetype;
        }
    }
    public bool InArena
    {
        get
        {
            return inArena;
        }
    }
    public bool InCombat
    {
        get
        {
            return inCombat;
        }
    }
    public int TutorialLevel
    {
        get
        {
            return tutorialLevel;
        }
        set
        {
            tutorialLevel = value;
        }
    }
    public bool EnemyDead
    {
        get
        {
            return enemyDead;
        }
        set
        {
            enemyDead = value;
        }
    }

}
