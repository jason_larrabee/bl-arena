﻿using UnityEngine;

public class GetTarget : MonoBehaviour {

    private RaycastHit hit;                             //Targetting ray
    private float distance;                             //Distance to the object
    private GameObject targetGameObject;                //Object being targetted
                                                        // Use this for initialization
    void Start () {
	
	}

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 fwd = this.transform.TransformDirection(Vector3.forward);
        if (Physics.Raycast(this.transform.position, fwd, out hit))
        {
            //Get all the information on the hit target
            distance = hit.distance;
            targetGameObject = hit.collider.gameObject;
        }
        else
        {
            distance = -1;
            targetGameObject = null;
        }
    }
    //***************************************************************************************************
    // ACCESSORS
    //***************************************************************************************************
    public float Distance
    {
        get
        {
            return distance;
        }
    }

    public GameObject Target
    {
        get
        {
            return targetGameObject;
        }
    }
}
