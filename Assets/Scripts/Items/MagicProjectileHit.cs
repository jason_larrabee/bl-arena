﻿using UnityEngine;


public class MagicProjectileHit : MonoBehaviour {

	void OnTriggerEnter(Collider col)
    {
        if (col.tag != "Enemy" && col.tag != "Player" && col.tag != "Weapon")
        {
            Destroy(this.gameObject);
        }
    }
}
