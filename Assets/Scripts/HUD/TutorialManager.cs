﻿using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour {
    //***************************************************************************************
    // TutorialManager.cs
    //
    // Handles the new player tutorial that walks the player through the beginning stage of the
    //  game
    //***************************************************************************************

    [SerializeField]
    private Text tutorText;                 //Text display on the tutorial window
    private string[] tutorials;             //Tutorial display strings

    //Player reference
    public Player_Manager pInfo;

    //***************************************************************************************
    // void Start()
    // 
    // Initializes the tutorial by locating the player reference(for calling the tutorial HUD iteself)
    // and initializes the tutorial strings
    //***************************************************************************************
    void Start()
    {
        //Set up the tutorials
        InitializeTutorialStrings();
    }
    // END: void Start() ********************************************************************

    //***************************************************************************************
    // void ShowTutorial(int tutNum)
    //  int tutNum: tutorial number to show
    //
    // Sets the tutorial text display to the corresponding tutorial in tutNum
    //***************************************************************************************
    public void ShowTutorial(int tutNum)
    {
        if (pInfo.InTutorial)
        {
            //Set the text on the Tutorial HUD
            tutorText.text = tutorials[tutNum];
            //Show the tutorial
            pInfo.gMan.ToggleTutorialPanel();
        }
        else return;
    }
    // END: void ShowTutorial(int tutNum) ***************************************************

    //***************************************************************************************
    // void InitializeTutorialStrings()
    //
    // Sets all the tutorial texts.
    //***************************************************************************************
    private void InitializeTutorialStrings()
    {
        //Initialize the array
        tutorials = new string[9];
        //Set the texts
        tutorials[0] = "Welcome to Badlands: Arena!\n\nHere you will see what you are made of and if you have what it takes to challenge the lord of the mountain!\nProceed down the corridor using W A S D to move.";
        tutorials[1] = "Getting there a little quicker.\n\nPress LEFT SHIFT to sprint down the hallway and use the SPACEBAR to jump around.\n\nUsing sprint will deplete your stamina over time and jumping will use stamina each time you jump.  Be sure to not run yourself out of stamina is it can take a few seconds to regenerate enough to do anything.  All vital stats, Health, Stamina, and Mana will regenerate over time.";
        tutorials[2] = "Interacting with the world around you.\n\nInteract with the door infront of you by walking up to it and presing R.  The R key is used to interact with all world objects including friendly NPCs such as merchants and guards.";
        tutorials[3] = "Buying and equipping.\n\nThere is a blacksmith on the right with a weapon for you.  Walk up to him and press the interact key(R) to open his shop window.  Click on the BUY button beside the Shortsword to purchase it.\nOnce purchased, open your inventory by pressing I.  You will see your new sword, left click on it to bring up the weapon's information.  With the details window up, we can see the items damage, value, and other information.  Clicking on the item icon will equip your weapon, causing a little E to appear on the item in your inventory.  Pressing the icon again will remove the equipped item and leave it in your inventory.\n\nGo ahead and equip the sword, then head over to the Arena Guard and speak with him to begin your first match.";
        tutorials[4] = "";
        tutorials[5] = "FIGHT!\n\nNow that you are ready for the arena, it is time to fight!  Inside is a scrawny thief who has chosen to carry out his sentance in the arena instead of prison.  To attack, press the left mouse button and your sword will swing.  You must make contact with your opponent to register a hit so aim well!  Right-clicking will attempt to block/dodge their attack which can really help when you are low on health.\n\nReturn to the lobby by talking to the Arena Guard when you have defeated your foe to complete your training.";
        tutorials[6] = "Magic and Skills\n\nSpeak to the mystic and purchase the staff and the Fireball skill.  Once purchased, equip the staff and then click on the Fireball skill. Click on the Fireball icon in the details window to learn the skill. Once learned, you can access your skills by pressing K.\nIn the skills window, click on a skill to bring up the items details on the right side or click directly on the icon to hotkey it to your shortcut bar.\n\nHINT: Hold CONTROL while clicking on an item to skip the detail step, or hold ALT while clicking a potion to use it directly from your inventory.";
        tutorials[7] = "With staff in hand and Fireball ready, enter the arena once more to use your new skill. Like with the sword, you can left click to perform a basic attack exept as a generic magical projectile.  Fireball is a projectile spell as well and will shoot straight out ahead of you when cast so be sure to keep that in mind if you are dealing with a moving target.";
        tutorials[8] = "WELL FOUGHT!\n\nWith these victories under your belt you are now ready to call yourself a gladiator.  Clicking OK will complete the tutorial and send you to the first true arena, do so when you are ready and good luck!.";
    }
    // END: void InitializeTutorialStrings() ************************************************
}
