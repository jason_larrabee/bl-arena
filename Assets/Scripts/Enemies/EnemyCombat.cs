﻿using UnityEngine;

public class EnemyCombat : MonoBehaviour {
    //*****************************************************************************************
    // EnemyCombat.cs
    //
    // Handles AI basic combat routines
    //*****************************************************************************************
    //Combatant Information
    //PlayerManager pInfo;                    //Player
    //EnemyManager eInfo;                     //NPC

    ////Class archetype
    //[SerializeField]
    //int classArchetype = 0;                 //0 - Melee
    //                                        //1 - Ranged
    //                                        //2 - Magic
    ////Type of attack being used
    //int attackType = -1;                    //-1 - Not Attacking
    //                                        //0 - Thrust
    //                                        //1 - Overhead
    //                                        //2 - Swipe Med
    //                                        //3 - Swipe Low
    //                                        //4 - Swipe high
    //                                        //5 - Shoot bow
    //                                        //6 - Cast Spell

    ////Flags
    //public bool isAttacking = false;	    //If enemy is actively attacking
    //public bool inRange = false;            //If in range to attack
    //public bool playerRangedAttack = false; //If the player is using a ranged attack
    //public bool isDodging = false;          //If enemy is dodging
    //public bool isRetreating = false;       //If enemy is retreating

    ////Timers
    //float attackCost = 10.0f;
    //float attackTimer = 2.0f;
    //float attackDelay = 2.0f;
    //float damageTimer = 0.0f;
    //float damageDelay = 1.0f;
    //float projTimer = -1.0f;
    //float projDelay = 2.5f;
  
    //private float attackRange = 0.0f;       //Set by class archetype
    //                                        //Melee = 2
    //                                        //Ranged = 30
    //                                        //Magic = 20

    //public Rigidbody attackProjectile;      //Projectile to use when atttack(either spell effect when magic or arrow when ranged)

    ////Waypoint manager
    //private GameObject[] waypoints;
    //private Vector3 retreatToPos;
    //private Vector3 dodgeToPos;

    ////Skill Pool
    //[SerializeField]
    //private string[] skills;                //Names of skills available to the npc
    //[SerializeField]
    //private Rigidbody[] skillEffects;       //SKill effects associated with the available skills
    //private string lastCastSkill;           //Name of the last skill cast

    ////***************************************************************************
    //// void Start()
    ////
    //// Sets all the player and enemy references
    ////***************************************************************************
    //void Start()
    //{
    //    //Get Player Information
    //    pInfo = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerManager>();
    //    //Get Enemy Information
    //    eInfo = GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyManager>();
    //    //Set the waypoints
    //    waypoints = GameObject.FindGameObjectsWithTag("ArenaWaypoint");

    //    //Set ranges and mods
    //    switch (classArchetype)
    //    {
    //        case 0: //Melee
    //            attackRange = 2.0f;
    //            break;
    //        case 1: //Ranged
    //            attackRange = 30.0f;
    //            attackDelay = 4.0f;
    //            attackCost = attackCost * 2;
    //            break;
    //        case 2: //Magic
    //            attackRange = 20.0f;
    //            break;
    //    }
    //}
    //// END: void Start() *******************************************************

    ////**************************************************************************
    //// void FixedUpdate()
    ////
    //// Called once per frame
    //// Checks if the player is in range of attack and calls attack method if able
    //// to do so
    ////**************************************************************************
    //void FixedUpdate()
    //{
    //    //Check if the player is in range
    //    CheckRange();
    //    //If in range, begin attack sequence
    //    if (inRange)
    //    {
    //        //Stop the NPC and look at the player
    //        eInfo.IsWalking = false;
    //        eInfo.navAgent.SetDestination(this.gameObject.transform.position);
    //        this.gameObject.transform.LookAt(pInfo.gameObject.transform.position);

    //        //If attacking with a bow, rotate the NPC slightly to point the bow at the player
    //        if(classArchetype == 1)
    //        {
    //            Vector3 targetRot = new Vector3(this.gameObject.transform.rotation.x, this.gameObject.transform.rotation.y + 45.0f);
    //            this.gameObject.transform.Rotate(targetRot);
    //        }

    //        //Make the attack call
    //        // - If NPC is a mage, then pull from mana instead of stamina
    //        if (classArchetype == 2)
    //        {
    //            if (eInfo.eVitals.Mana >= attackCost && attackTimer >= attackDelay && !isAttacking)
    //            {
    //                //Check if we have enough mana to cast a skill
    //                if (eInfo.eVitals.Mana >= 15.0f)
    //                {
    //                    //Do we need to heal?
    //                    if (eInfo.eVitals.healthPercent < 50.0f)
    //                    {
    //                        float i = Random.Range(0, 100);
    //                        if (i > eInfo.eVitals.healthPercent)
    //                        {
    //                            attackTimer = 0.0f;
    //                            CastSkill("Heal");
    //                        }
    //                        else
    //                        {
    //                            int skill = Random.Range(0, 10);
    //                            if(skill < skills.Length)
    //                            {
    //                                attackTimer = 0.0f;
    //                                CastSkill(skills[skill]);
    //                            }

    //                        }
    //                    }
    //                    else
    //                    {
    //                        int skill = Random.Range(0, 10);
    //                        if (skill < skills.Length)
    //                        {
    //                            attackTimer = 0.0f;
    //                            CastSkill(skills[skill]);
    //                        }

    //                    }
    //                }
    //                else
    //                {
    //                    attackTimer = 0.0f;
    //                    Attack();
    //                }
    //            }
    //        }
    //        else
    //        {
    //            if (eInfo.eVitals.Stamina >= attackCost && attackTimer >= attackDelay && !isAttacking)
    //            {
    //                attackTimer = 0.0f;
    //                Attack();
    //            }
    //        }
    //    }
    //    //If not in range and not dodging or retreating, move to range
    //    else
    //    {
    //        //If we aren't dodging an attack, attempt to move into range
    //        if (!isDodging && !isRetreating)
    //        {
    //            MoveToRange();
    //        }
    //    }

    //    //Check if we should attempt to dodge an attack
    //    if(playerRangedAttack)
    //    {
    //        if (eInfo.eVitals.Stamina >= 10)
    //        {
    //            //Decide if we want to dodge
    //            if (eInfo.eVitals.staminaPercent >= 50.0)
    //            {
    //                AttemptToDodge();
    //            }
    //            else if (eInfo.eVitals.staminaPercent >= 25.0 && eInfo.eVitals.staminaPercent < 50.0f)
    //            {
    //                //50% chance to attempt to dodge
    //                int i = Random.Range(0, 2);
    //                if (i == 0)
    //                {
    //                    AttemptToDodge();
    //                }
    //            }
    //            else
    //            {
    //                //25% chance to attempt
    //                int i = Random.Range(0, 5);
    //                if(i == 0)
    //                {
    //                    AttemptToDodge();
    //                }
    //            }
    //        }
    //        playerRangedAttack = false;
    //    }

    //    //Check if we have reached our dodge destination
    //    if(isDodging)
    //    {
    //        Vector3 dist = dodgeToPos - eInfo.gameObject.transform.position;
    //        if (Mathf.Abs(dist.x) < 2.0f && Mathf.Abs(dist.z) < 2.0f)
    //        {
    //            isDodging = false;
    //            eInfo.navAgent.speed = eInfo.eStats.modDexterity / 2.0f;
    //            eInfo.navAgent.acceleration = eInfo.navAgent.acceleration / 3.0f;
    //        }
    //    }

    //    ////Check if we have reached our retreat destination
    //    //if(isRetreating)
    //    //{
    //    //    Vector3 dist = retreatToPos - eInfo.gameObject.transform.position;
    //    //    if (Mathf.Abs(dist.x) < 2.0f && Mathf.Abs(dist.z) < 2.0f)
    //    //    {
    //    //        isRetreating = false;
    //    //        eInfo.IsWalking = false;
    //    //        eInfo.navAgent.speed = eInfo.eStats.modDexterity / 2.0f;
    //    //        eInfo.navAgent.acceleration = eInfo.navAgent.acceleration / 2.0f;
    //    //    }
    //    //}

    //    //Check our timers
    //    //Attack timer - how long before next attack
    //    if (attackTimer < attackDelay)
    //    {
    //        attackTimer += Time.deltaTime;
    //    }
    //    //Swing delay for doing damage
    //    if (damageTimer > 0.0f)
    //    {
    //        damageTimer += Time.deltaTime;
    //        if (damageTimer >= damageDelay)
    //        {
    //            if (inRange && attackRange == 2.0f)
    //            {
    //                HitTarget();
    //            }
    //            damageTimer = 0.0f;
    //            isAttacking = false;
    //            attackType = -1;
    //        }
    //    }
    //    //Animation delay for spawning projectiles
    //    if (projTimer >= 0.0f)
    //    {
    //        projTimer += Time.deltaTime;
    //        if (projTimer >= projDelay)
    //        {
    //            ShootProjectile();
    //            projTimer = -1.0f;
    //            isAttacking = false;
    //            attackType = -1;
    //        }
    //    }
    //}
    //// END: void FixedUpdate() *************************************************

    ////**************************************************************************
    //// void CheckRange()
    ////
    //// Determines the range to the player, returns true if in range of attack
    //// false if not
    ////**************************************************************************
    //private void CheckRange()
    //{
    //    //Determine the distance to the player
    //    Vector3 dtp = pInfo.gameObject.transform.position - eInfo.gameObject.transform.position;
    //    //Check if the player is within range
    //    if (Mathf.Abs(dtp.x) < attackRange && Mathf.Abs(dtp.z) < attackRange)
    //    {
    //        //if (attackRange != 2.0f && Mathf.Abs(dtp.x) < (attackRange / 2) && Mathf.Abs(dtp.z) < (attackRange / 2))
    //        //{
    //        //    Retreat();
    //        //    inRange = false;
    //        //}
    //        //else
    //        //{
    //            inRange = true;
    //        //}


    //    }
    //    else
    //    {
    //        inRange = false;
    //    }
 
    //}
    //// END: bool CheckRange() **************************************************

    ////**************************************************************************
    //// void Attack()
    ////
    //// Attempts to hit player with the equipped weapon
    ////**************************************************************************
    //private void Attack()
    //{
        
    //    isAttacking = true;
    //    //Melee attack
    //    if (classArchetype == 0)
    //    {
    //        eInfo.eVitals.Stamina -= attackCost;
    //        attackType = Random.Range(0, 4);
    //        damageTimer += Time.deltaTime;
    //    }
    //    //Ranged attacks
    //    else if(classArchetype == 1)
    //    {
    //        eInfo.eVitals.Stamina -= attackCost;
    //        projTimer = 0.0f;
    //        attackType = 5;
    //    }
    //    //Magic Attacks
    //    else if (classArchetype == 2)
    //    {
    //        eInfo.eVitals.Mana -= attackCost;
    //        projTimer = 0.0f;
    //        attackType = 6;
    //    }
    //}
    //// END: void Attack() ******************************************************

    ////**************************************************************************
    //// void Retreat()
    ////
    //// Picks a random arena waypoint and runs to it
    ////**************************************************************************
    //private void Retreat()
    //{
    //    if (!isRetreating)
    //    {
    //        isRetreating = true;
    //        int r = Random.Range(0, waypoints.Length);
    //        retreatToPos = waypoints[r].transform.position;
    //        eInfo.IsWalking = true;
    //        eInfo.navAgent.speed = (eInfo.eStats.modDexterity / 2) * 1.25f;
    //        eInfo.navAgent.acceleration = eInfo.navAgent.acceleration * 2;
    //        eInfo.navAgent.SetDestination(retreatToPos);
    //    }
    //}
    //// END: void Retreat() *****************************************************

    ////**************************************************************************
    //// void HitTarget()
    ////
    //// Called when an enemy's strike hits the player
    ////**************************************************************************
    //public void HitTarget()
    //{
    //    //Determines basic attack damage based on type of attack
    //    int damage;   //Total damage done
    //    int dif;      //Potential damage
    //    //Melee Attack
    //    if (attackType >= 0 && attackType <= 4)
    //    {
    //        dif = eInfo.eStats.modMeleeAttack - pInfo.pStats.modPhysDefense;
    //        if (dif > 0)
    //        {
    //            damage = Random.Range(1, dif);
    //        }
    //        else
    //        {
    //            damage = Random.Range(1, 5);
    //        }
    //        pInfo.pVitals.Health -= damage;

    //    }

    //    //Ranged Attack
    //    if (classArchetype == 1)
    //    {
    //        dif = eInfo.eStats.modRangedAttack - pInfo.pStats.modPhysDefense;
    //        if (dif > 0)
    //        {
    //            damage = Random.Range(0, dif);
    //        }
    //        else
    //        {
    //            damage = Random.Range(0, 5);
    //        }
    //        pInfo.pVitals.Health -= damage;
    //    }
    //    //Magic Attack
    //    else if (classArchetype == 2)
    //    {
    //        dif = eInfo.eStats.modMagicAttack - pInfo.pStats.modMagicDefense;
    //        if (dif > 0)
    //        {
    //            damage = Random.Range(0, dif);
    //        }
    //        else
    //        {
    //            damage = Random.Range(0, 5);
    //        }
    //        pInfo.pVitals.Health -= damage;
    //    }
    //}
    //// END: void HitTarget() ****************************************************

    ////***************************************************************************
    //// void ShootProjectile()
    ////
    //// Spawns and shoots an attack projectile
    ////***************************************************************************
    //private void ShootProjectile()
    //{
    //    //Set casting position
    //    GameObject projSpawn = GameObject.FindGameObjectWithTag("EnemyProjectileSpawn");
        
    //    //Cast the projectile
    //    Rigidbody projectile = (Rigidbody)Instantiate(attackProjectile, projSpawn.transform.position, projSpawn.transform.rotation);
    //    projectile.transform.LookAt(pInfo.gameObject.transform);
    //    projectile.velocity = projectile.transform.forward * 40;
    //}
    //// END: void ShootProjectile() **********************************************

    ////***********************************************************************************************************
    //// Navigation Methods
    ////***********************************************************************************************************
    ////***************************************************************************
    //// void MoveToRange()
    ////
    //// Moves the enemy to their ideal attack range
    ////***************************************************************************
    //private void MoveToRange()
    //{
    //    eInfo.navAgent.speed = eInfo.eStats.modDexterity / 2;
    //    eInfo.navAgent.SetDestination(pInfo.gameObject.transform.position);
    //    eInfo.IsWalking = true;
    //}
    //// END: void MoveToRange() **************************************************

    ////******************************************************************
    //// void AttemptToDodge()
    ////
    //// Determines if dodging and in what direction
    ////******************************************************************
    //private void AttemptToDodge()
    //{
    //    //Find our direction
    //    int dodgeDir = Random.Range(0, 4);
    //    //If failed attempt, return
    //    if (dodgeDir > 2)
    //    {
    //        return;
    //    }
    //    //Success so make the dodge
    //    else
    //    {
    //       DodgeAttack(dodgeDir);
    //    }
    //}
    //// END: void AttemptToDodge() **************************************

    ////******************************************************************
    //// void DodgeAttack(int d)
    ////  int d: direction to dodge
    ////
    //// Dodges in the direction given by d at an increased rate of speed
    ////******************************************************************
    //private void DodgeAttack(int d)
    //{
    //    //Takes dodgeDir(d) and moves the enemy accordingly
    //    Vector3 targetPos = eInfo.gameObject.transform.position;
    //    switch (d)
    //    {
    //        case 0://Left
    //            targetPos.z += 5;
    //            if (!eInfo.navAgent.SetDestination(targetPos))
    //            {
    //                isDodging = false;
    //            }
    //            eInfo.navAgent.acceleration = eInfo.navAgent.acceleration * 3;
    //            eInfo.navAgent.speed = eInfo.navAgent.speed * 1.5f;
    //            dodgeToPos = targetPos;
    //            isDodging = true;
    //            eInfo.eVitals.Stamina -= 10;
    //            break;
    //        case 1://Back
    //            targetPos.x -= 5;
    //            if (!eInfo.navAgent.SetDestination(targetPos))
    //            {
    //                isDodging = false;
    //            }
    //            eInfo.navAgent.acceleration = eInfo.navAgent.acceleration * 3;
    //            eInfo.navAgent.speed = eInfo.navAgent.speed * 1.5f;
    //            dodgeToPos = targetPos;
    //            isDodging = true;
    //            eInfo.eVitals.Stamina -= 10;
    //            break;
    //        case 2://Right
    //            targetPos.z -= 5;
    //            if (!eInfo.navAgent.SetDestination(targetPos))
    //            {
    //                isDodging = false;
    //            }
    //            eInfo.navAgent.acceleration = eInfo.navAgent.acceleration * 3;
    //            eInfo.navAgent.speed = eInfo.navAgent.speed * 1.5f;
    //            dodgeToPos = targetPos;
    //            isDodging = true;
    //            eInfo.eVitals.Stamina -= 10;
    //            break;
    //    }
    //}
    //// END: void DodgeAttack(int d) ************************************

    ////**************************************************************************************************************
    //// SKILLS
    ////**************************************************************************************************************
    ////***************************************************************************
    //// void CastSkill(string snNme)
    ////  string sName: number of the skill to cast
    ////
    //// Casts the requested skill.
    ////***************************************************************************
    //private void CastSkill(string sName)
    //{

    //    int sNum = 0; //Number of skill being cast
    //    switch(sName)
    //    {
    //        case "Fireball": 
    //            //Determine which number skill is being cast
    //            for (int i = 0; i < skills.Length; i++)
    //            {
    //                if(skills[i] == sName)
    //                {
    //                    sNum = i;
    //                    break;
    //                }
    //            }
    //            //Remove mana
    //            eInfo.eVitals.Mana -= 15.0f;
    //            lastCastSkill = sName;

    //            //Set casting position
    //            GameObject projSpawn = GameObject.FindGameObjectWithTag("EnemyProjectileSpawn");

    //            //Cast the projectile
    //            Rigidbody projectile = (Rigidbody)Instantiate(skillEffects[sNum], projSpawn.transform.position, projSpawn.transform.rotation);
    //            projectile.transform.LookAt(pInfo.gameObject.transform);
    //            projectile.velocity = projectile.transform.forward * 40;

    //            //Update player UI
    //            pInfo.pHUD.UpdateEnemyHUD();
    //            break;
    //        case "IceShards":
    //            //Determine which number skill is being cast
    //            for (int i = 0; i < skills.Length; i++)
    //            {
    //                if (skills[i] == sName)
    //                {
    //                    sNum = i;
    //                    break;
    //                }
    //            }
    //            //Remove mana
    //            eInfo.eVitals.Mana -= 15.0f;
    //            lastCastSkill = sName;

    //            //Set casting position
    //            GameObject shardSpawn = GameObject.FindGameObjectWithTag("EnemyProjectileSpawn");

    //            //Cast the projectile
    //            Rigidbody shardProj = (Rigidbody)Instantiate(skillEffects[sNum], shardSpawn.transform.position, shardSpawn.transform.rotation);
    //            shardProj.transform.LookAt(pInfo.gameObject.transform);
    //            shardProj.velocity = shardProj.transform.forward * 40;

    //            //Update player UI
    //            pInfo.pHUD.UpdateEnemyHUD();
    //            break;

    //        case "Heal":
    //            //Determine which number skill is being cast
    //            for (int i = 0; i < skills.Length; i++)
    //            {
    //                if (skills[i] == sName)
    //                {
    //                    sNum = i;
    //                    break;
    //                }
    //            }
    //            //Remove mana
    //            eInfo.eVitals.Mana -= 15.0f;
    //            lastCastSkill = sName;

    //            Instantiate(skillEffects[sNum], this.gameObject.transform.position, this.gameObject.transform.rotation);
    //            eInfo.eVitals.Health += Random.Range(0, (15 + eInfo.eStats.modMagicAttack + (eInfo.eStats.modIntelligence / 10)));
    //            break;
    //        case "LightningStrike":
    //            //Determine which number skill is being cast
    //            for (int i = 0; i < skills.Length; i++)
    //            {
    //                if (skills[i] == sName)
    //                {
    //                    sNum = i;
    //                    break;
    //                }
    //            }
    //            //Remove mana
    //            eInfo.eVitals.Mana -= 15.0f;
    //            lastCastSkill = sName;

    //            Instantiate(skillEffects[sNum], pInfo.gameObject.transform.position, pInfo.gameObject.transform.rotation);
    //            pInfo.pVitals.Health -= Random.Range(0, (20 + eInfo.eStats.modMagicAttack + (eInfo.eStats.modIntelligence / 10)));
    //            break;
    //        case "AcidicCloud":
    //            //Determine which number skill is being cast
    //            for (int i = 0; i < skills.Length; i++)
    //            {
    //                if (skills[i] == sName)
    //                {
    //                    sNum = i;
    //                    break;
    //                }
    //            }
    //            //Remove mana
    //            eInfo.eVitals.Mana -= 15.0f;
    //            lastCastSkill = sName;

    //            Instantiate(skillEffects[sNum], pInfo.gameObject.transform.position, pInfo.gameObject.transform.rotation);
    //            //Difference between total potential damage and enemies defense level
    //            int d = (eInfo.eStats.modMagicAttack + 20 + (eInfo.eStats.modIntelligence / 10)) - pInfo.pStats.modMagicDefense;
    //            //if positive, pull random number in range of 0 and difference for actual damage
    //            if (d > 0)
    //            {
    //                int damage = Random.Range(0, d);
    //                //Set the dot
    //                pInfo.pVitals.DamageOverTime(damage, 5);
    //            }
    //            //pInfo.pVitals.Health -= Random.Range(0, (20 + eInfo.eStats.modMagicAttack + (eInfo.eStats.modIntelligence / 10)));
    //            break;
    //    }
    //}
    //// END: CastSkill(string sName) *********************************************

    //public void SkillHitTarget()
    //{
    //    switch (lastCastSkill)
    //    {
    //        case "Fireball":
    //            //Difference between total potential damage and players defense level
    //            int d = (eInfo.eStats.modMagicAttack + 30 + (eInfo.eStats.modIntelligence / 10)) - pInfo.pStats.modMagicDefense;
    //            //if positive, pull random number in range of 0 and difference for actual damage
    //            if (d > 0)
    //            {
    //                int damage = Random.Range(0, d);
    //                //Remove the health
    //                pInfo.pVitals.Health -= damage;
    //            }
    //            break;
    //    }
    //}

    //***************************************************************************
    // ACCESSORS
    //***************************************************************************
    // int attackType
    //public int AttackType
    //{
    //    get
    //    {
    //        return attackType;
    //    }
    //    set
    //    {
    //        attackType = value;
    //    }
    //}
    //// bool isAttacking
}
