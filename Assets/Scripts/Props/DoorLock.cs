﻿using UnityEngine;

public class DoorLock : MonoBehaviour {

    [SerializeField]
    private bool locked = false;

    public bool Locked
    {
        get
        {
            return locked;
        }
        set
        {
            locked = value;
        }
    }
}
