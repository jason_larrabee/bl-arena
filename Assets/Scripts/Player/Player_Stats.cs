﻿using UnityEngine;

public class Player_Stats : MonoBehaviour {
    //*****************************************************************************************
    // Player_Stats.cs
    //  Attatch To: Player Game Object
    //
    // Tracks all of the player’s statistics.
    // Raises and lowers stats according to skills and equipment.
    // Calculates secondary and dependent stats when changes are incurred.
    // Raises a stat when told to do so by the Player Experience script.
    //*****************************************************************************************
    //External References
    [SerializeField]
    private Player_Manager pInfo;

    // VITALS
    public float[] stats_vitals;
    // - 0/1 - Health/Max
    // - 2/3 - Stamina/Max
    // - 4/5 - Mana/Max

    // PRIMARY STATS
    private float[] stats_primary;
    // - 0 - Strength
    // - 1 - Dexterity
    // - 2 - Intelligence

    // SECONDARY STATS
    private float[] stats_secondary;
    // - 0 - Melee Attack
    // - 1 - Ranged Attack
    // - 2 - Magic Attack
    // - 3 - Physical Defense
    // - 4 - Magical Defense

    // DERIVED STATS
    // These are the final computed versions of the secondary stats with all modifiers applied.  These are the stats accessed during combat checks and in the UI.
    private float[] stats_derived;
    // - 0 – Max Health
    // - 1 – Max Stamina
    // - 2 – Max Mana
    // - 3 – Strength
    // - 4 – Dexterity
    // - 5 - Intelligence
    // - 6 - Melee Attack
    // - 7 - Ranged Attack
    // - 8 - Magic Attack
    // - 9 - Physical Defense
    // - 10 - Magical Defense 

    // EFFECT BONUS – This is the cumulative mods applied to each of the stats.  The bonuses are from equipment, spells, or skills.
    private float[] totalMods;
    // - 0 – Max Health
    // - 1 – Max Stamina
    // - 2 – Max Mana
    // - 3 – Strength
    // - 4 – Dexterity
    // - 5 - Intelligence
    // - 6 - Melee Attack
    // - 7 - Ranged Attack
    // - 8 - Magic Attack
    // - 9 - Physical Defense
    // - 10 - Magical Defense 
    private float[] itemMods;
    // - 0 – Max Health
    // - 1 – Max Stamina
    // - 2 – Max Mana
    // - 3 – Strength
    // - 4 – Dexterity
    // - 5 - Intelligence
    // - 6 - Melee Attack
    // - 7 - Ranged Attack
    // - 8 - Magic Attack
    // - 9 - Physical Defense
    // - 10 - Magical Defense 

    //Arena Stats
    private int wins = 0;
    private int losses = 0;

    //Flags
    private bool isDead = false;


    //******************************************************************************************
    // void Start()
    //
    // Initializes the players stats.
    // If new game then it creates a basic character from the default templates (Melee, Ranged, Magical)
    // If loading game, sets the stats to their saved values
    //******************************************************************************************
    void Start ()
    {

        //Initialize the stats
        stats_vitals = new float[6];
        stats_primary = new float[3];
        stats_secondary = new float[5];
        stats_derived = new float[11];
        totalMods = new float[11];
        itemMods = new float[11];
        //Set up the player
        // - If its a new game create the default
        if (pInfo.IsNewGame)
        {
            //Use the archetype chosen at game start
            switch (pInfo.Archetype)
            {
                //Melee
                case 0:
                    //Vitals
                    stats_vitals[0] = 150;
                    stats_vitals[1] = 150;
                    stats_vitals[2] = 100;
                    stats_vitals[3] = 100;
                    stats_vitals[4] = 100;
                    stats_vitals[5] = 100;
                    //Primary
                    stats_primary[0] = 15;
                    stats_primary[1] = 10;
                    stats_primary[2] = 10;
                    //Secondary
                    stats_secondary[0] = 15;
                    stats_secondary[1] = 10;
                    stats_secondary[2] = 10;
                    stats_secondary[3] = 15;
                    stats_secondary[4] = 10;
                    break;
                //Ranged
                case 1:
                    //Vitals
                    stats_vitals[0] = 100;
                    stats_vitals[1] = 100;
                    stats_vitals[2] = 150;
                    stats_vitals[3] = 150;
                    stats_vitals[4] = 100;
                    stats_vitals[5] = 100;
                    //Primary
                    stats_primary[0] = 10;
                    stats_primary[1] = 15;
                    stats_primary[2] = 10;
                    //Secondary
                    stats_secondary[0] = 10;
                    stats_secondary[1] = 15;
                    stats_secondary[2] = 10;
                    stats_secondary[3] = 15;
                    stats_secondary[4] = 10;
                    break;
                //Magic
                case 2:
                    //Vitals
                    stats_vitals[0] = 100;
                    stats_vitals[1] = 100;
                    stats_vitals[2] = 100;
                    stats_vitals[3] = 100;
                    stats_vitals[4] = 150;
                    stats_vitals[5] = 150;
                    //Primary
                    stats_primary[0] = 10;
                    stats_primary[1] = 10;
                    stats_primary[2] = 15;
                    //Secondary
                    stats_secondary[0] = 10;
                    stats_secondary[1] = 10;
                    stats_secondary[2] = 15;
                    stats_secondary[3] = 10;
                    stats_secondary[4] = 15;
                    break;
                //Custom
                case 3:
                    //Stats will be imported from character creation
                    break;
            }
            //Derived
            CalculateDerivedStats();
        }
        // - If loading game, get saved data and set it
        else
        {

        }

        //Update GUI
        pInfo.gMan.UpdateVitalPanel();
        pInfo.gMan.UpdateCharPanel();
 	}
    // END: void Start() ***********************************************************************
	
    //******************************************************************************************
    // void CalculateDerivedStats()
    //
    // Calculates and sets derived stats based on primary  and secondary values
    //******************************************************************************************
    private void CalculateDerivedStats()
    {
        
        //Primary Stats
        stats_derived[3] = stats_primary[0] + totalMods[3];         // Strength
        stats_derived[4] = stats_primary[1] + totalMods[4];         // Dexterity
        stats_derived[5] = stats_primary[2] + totalMods[5];         // Intelligence
        // - Calculate vital mods from primary stats
        totalMods[0] = (stats_derived[3] / 5.0f) + itemMods[0];                     //MaxHealth Mod = Strength/5;
        totalMods[1] = (stats_derived[4] / 5.0f) + itemMods[1];                     //MaxStamina Mod = Dexterity/5;
        totalMods[2] = (stats_derived[5] / 5.0f) + itemMods[2];                     //MaxMana Mod = Intelligence/5;
        // - Calculate secondary mods from primary stats
        totalMods[6] = ((stats_derived[3] + stats_derived[4]) / 2.0f) + itemMods[6];   // Melee Attack = (Strength + Dexterity)/2
        totalMods[7] = ((stats_derived[3] + stats_derived[4]) / 2.0f) + itemMods[7];   // Ranged Attack = (Strength + Dexterity)/2
        totalMods[8] = (stats_derived[5]) + itemMods[8];                            // Magic Attack = Intelligence
        totalMods[9] = ((stats_derived[3] + stats_derived[4]) / 3.0f) + itemMods[9];   // Physical Defense = (Strength + Dexterity)/3
        totalMods[10] = ((2.0f * stats_derived[5]) / 3.0f) + itemMods[10];                 // Magic Defense = (2 * Intelligence)/3

        //Vital Stats
        stats_derived[0] = stats_vitals[1] + totalMods[0];          // Max Health
        stats_derived[1] = stats_vitals[3] + totalMods[1];          // Max Stamina
        stats_derived[2] = stats_vitals[5] + totalMods[2];          // Max Mana

        //Secondary Stats
        stats_derived[6] = stats_secondary[0] + totalMods[6];       // Melee Attack
        stats_derived[7] = stats_secondary[1] + totalMods[7];       // Ranged Attack
        stats_derived[8] = stats_secondary[2] + totalMods[8];       // Magic Attack
        stats_derived[9] = stats_secondary[3] + totalMods[9];       // Physical Defense
        stats_derived[10] = stats_secondary[4] + totalMods[10];     // Magical Defense

        pInfo.gMan.UpdateCharPanel();
    }
    // END: void CalculateDerivedStats() *******************************************************
    public void UpdatePlayerStats()
    {
        CalculateDerivedStats();
    }
    //******************************************************************************************
    // void IncreaseStat(int statType, int statLoc, float amount)
    //  int statType: 0 – vital, 1 – primary, 2 – secondary
    //  int statLoc: array location of the stat to raise
    //  float amount: amount to raise the stat by
    //
    // Increases a stat permanently by the specified amount
    //******************************************************************************************
    public void IncreaseStat(int statType, int statLoc, float amount)
    {
        switch (statType)
        {
            case 0:
                stats_vitals[statLoc] += amount;
                if (statLoc == 1) pInfo.gMan.ShowMessage("Maximum Health increased by " + amount + "!");
                if (statLoc == 3) pInfo.gMan.ShowMessage("Maximum Stamina increased by " + amount + "!");
                if (statLoc == 5) pInfo.gMan.ShowMessage("Maximum Mana increased by " + amount + "!");
                break;

            case 1:
                stats_primary[statLoc] += amount;
                if (statLoc == 0) pInfo.gMan.ShowMessage("Strength increased by " + amount + "!");
                if (statLoc == 1) pInfo.gMan.ShowMessage("Dexterity increased by " + amount + "!");
                if (statLoc == 2) pInfo.gMan.ShowMessage("Mana increased by " + amount + "!");
                break;

            case 2:
                stats_secondary[statLoc] += amount;
                if (statLoc == 0) pInfo.gMan.ShowMessage("Melee Attack increased by " + amount + "!");
                if (statLoc == 1) pInfo.gMan.ShowMessage("Ranged Attack increased by " + amount + "!");
                if (statLoc == 2) pInfo.gMan.ShowMessage("Magic Attack increased by " + amount + "!");
                if (statLoc == 3) pInfo.gMan.ShowMessage("Physical Defense increased by " + amount + "!");
                if (statLoc == 4) pInfo.gMan.ShowMessage("Magic Defense increased by " + amount + "!");
                break;
        }

        CalculateDerivedStats();
       
    }
    // END: void IncreaseStat(int statType, int statLoc, float amount)**************************

    //******************************************************************************************
    // void ModifyVital(int vital, float amount)
    //  int vital: Vital stat to modify
    //  float amount: amount to modify by (+ remove damage, - apply damage)
    //
    // Modifies the vital stat by the amount passed
    // Used to apply or remove damage from the player
    //******************************************************************************************
    public void ModifyVital(int vital, float amount)
    {
        //Apply the modification
        stats_vitals[vital] += amount;
       
        //Limit to the derived maximum and 0
        if (stats_vitals[vital] > stats_derived[(vital/2)])
        {
            stats_vitals[vital] = stats_derived[(vital / 2)];
        }
        if (stats_vitals[vital] <= 0.0f)
        {
            stats_vitals[vital] = 0.0f;
        }

        //Give xp for taking damages(if damage is taken)
        if (amount < 0.0f)
        {
            pInfo.pXP.GiveXP(0, vital/2, Mathf.Abs(amount));
        }

        //Determine life/death status of the player
        if (stats_vitals[0] > 0.0f)
        {
            isDead = false;
        }
        else
        {
            isDead = true;
        }

        //Update the GUI
        pInfo.gMan.UpdateVitalPanel();
    }

    //******************************************************************************************
    // PROPERTIES
    //******************************************************************************************
    public float Health
    {
        get
        {
            return stats_vitals[0];
        }
    }
    public float MaxHealth
    {
        get
        {
            return stats_derived[0];
        }
    }
    public float Stamina
    {
        get
        {
            return stats_vitals[2];
        }
    }
    public float MaxStamina
    {
        get
        {
            return stats_derived[1];
        }
    }
    public float Mana
    {
        get
        {
            return stats_vitals[4];
        }
    }
    public float MaxMana
    {
        get
        {
            return stats_derived[2];
        }
    }
    public float Strength
    {
        get
        {
            return stats_derived[3];
        }
    }
    public float Dexterity
    {
        get
        {
            return stats_derived[4];
        }
    }
    public float Intelligence
    {
        get
        {
            return stats_derived[5];
        }
    }
    public float MeleeAttack
    {
        get
        {
            return stats_derived[6];
        }
    }
    public float RangedAttack
    {
        get
        {
            return stats_derived[7];
        }
    }
    public float MagicAttack
    {
        get
        {
            return stats_derived[8];
        }
    }
    public float PhysicalDefense
    {
        get
        {
            return stats_derived[9];
        }
    }
    public float MagicDefense
    {
        get
        {
            return stats_derived[10];
        }
    }
    public float[] Mods
    {
        get
        {
            return totalMods;
        }
        set
        {
            totalMods = value;
        }
    }

    public float[] ItemMods
    {
        get
        {
            return itemMods;
        }
        set
        {
            itemMods = value;
        }
    }
    public int Wins
    {
        get
        {
            return wins;
        }
        set
        {
            wins = value;
        }
    }
    public int Losses
    {
        get
        {
            return losses;
        }
        set
        {
            losses = value;
        }
    }
    public bool IsDead
    {
        get
        {
            return isDead;
        }
 
    }
}
