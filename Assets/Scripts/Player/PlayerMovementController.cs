﻿using UnityEngine;

public class PlayerMovementController : MonoBehaviour {
//******************************************************************
//  PlayerMovementController.cs
//  Handles all player movements
//
//  Movement Speed: Based on the players dexterity stat and is equal 
//                  to their modified dexterity
//  Sprint Speed:   Based on movement speed and is equal to 1.5x 
//                  base movement speed
//  Jump Height:    Based on the players strength and dexterity and
//                  is equal to the average value of their modified
//                  strength and modified dexterity times 300
//******************************************************************
    [SerializeField]
    private Rigidbody rb;
    [SerializeField]
    private Player_Manager pInfo;

    
    private float speed;
    private float speed_Sprint;
    private float jumpForce;
    //Action Costs
    private float sprintCost = 5.0f;
    private float jumpCost = 7.0f;

    //Movement flags
    private bool isJumping = false;
    private bool isSprinting = false;

    //Timers
    private float jumpTimer = -1.0f;
    private float sprintTimer = -1.0f;

    
    void FixedUpdate()
    {
        //Final modified speeds
        float speed_Final = 0.0f;
        speed = pInfo.pStats.Dexterity;
        speed_Sprint = speed * 1.5f;
        //Final jump forces
        jumpForce = (((pInfo.pStats.Strength + pInfo.pStats.Dexterity)/2) * 20);

        //If the player is spriting add sprint speed to the player
        if (Input.GetAxis("Sprint") > 0 && pInfo.pStats.Stamina > sprintCost)
        {
            speed_Final = speed_Sprint;
            //If we have just started the sprint, remove starting stamina
            if (!isSprinting)
            {
                pInfo.pStats.ModifyVital(2, sprintCost);
            }
            //Set sprinting to true
            isSprinting = true;
        }
        //Otherwise use base speed
        else
        {
            speed_Final = speed;
            isSprinting = false;
        }

        //Get the player movement input
        float translationForward = 0.0f;
        float translationStrafe = 0.0f;
        float jump = 0.0f;
        
        translationForward = Input.GetAxis("Vertical") * speed_Final;
        translationStrafe = Input.GetAxis("Horizontal") * (speed_Final / 2);
        jump = Input.GetAxis("Jump") * jumpForce;
        
        //Convert to usable amount
        translationForward *= Time.deltaTime;
        translationStrafe *= Time.deltaTime;
        //Apply movement changes
        if (translationForward != 0.00f && !isJumping)
        {
            rb.transform.Translate(0, 0, translationForward);
        }
        if (translationStrafe != 0.00f && !isJumping)
        {
            rb.transform.Translate(translationStrafe, 0, 0);
        }


        //If we have jumped
        if (jump > 0.0f && !isJumping && pInfo.pStats.Stamina > jumpCost)
        {
            rb.AddForce(Vector3.up * jump);
            rb.AddRelativeForce(Vector3.forward * translationForward * 2000);
            rb.AddRelativeForce(Vector3.right * translationStrafe * 2000);

            pInfo.pStats.ModifyVital(2, -jumpCost);
            isJumping = true;
            jumpTimer = 0.0f;
        }
        //Check our jump timer, update if necessary
        if (jumpTimer >= 0.0f)
        {
            UpdateJumpTimer();
        }

        //Check sprint timer
        if (isSprinting)
        {
            UpdateSprintTimer();
        }

    }

    //************************************************************************************
    // void UpdateJumpTimer()
    //
    // Tracks and updates the jump timer to prevent "jump-spam"
    //************************************************************************************
    private void UpdateJumpTimer()
    {
        jumpTimer += Time.deltaTime;
        if (jumpTimer > 1.0f)
        {
            isJumping = false;
            jumpTimer = -1.0f;
        }
    }
    // END: void UpdateJumpTimer() *******************************************************

    //************************************************************************************
    // void UpdateSprintTimer()
    //
    // Tracks and updates the sprint timer to subtract the sprint cost every second
    //************************************************************************************
    private void UpdateSprintTimer()
    {
        //Increment timer
        sprintTimer += Time.deltaTime;
        //At one second, remove the sprintCost from the players stamina and reset the tiemr
        if (sprintTimer > 1.0f)
        {
            pInfo.pStats.ModifyVital(2, -sprintCost);
            sprintTimer = 0.0f;
        }
    }
    // END: void UpdateJumpTimer() *******************************************************
    //************************************************************************************
    // ACCESSORS
    //************************************************************************************

    // bool isJumping
    public bool IsJumping
    {
        get
        {
            return isJumping;
        }
        set
        {
            isJumping = value;
        }
    }
    // - bool isSprinting
    public bool IsSprinting
    {
        get
        {
            return isSprinting;
        }
    }
}

