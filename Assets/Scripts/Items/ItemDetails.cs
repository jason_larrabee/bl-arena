﻿using UnityEngine;


public class ItemDetails : MonoBehaviour {
    //******************************************************************************************************
    // ItemDetails.cs
    //
    // Contains all of an items details
    //******************************************************************************************************
    [SerializeField]
    private Player_Manager pInfo;    //Reference to the player
    [SerializeField]
    private string itemName;         //Name of the item
    [SerializeField]
    private Sprite itemIcon;		 //Items icon sprite
    [SerializeField]
    private int itemCost;			 //Cost of item
    [SerializeField]
    private int itemType;            //Type of item
                                     //0-Shield
                                     //1-Weapon
                                     //2-Armor
                                     //3-Potion
                                     //4-Skill
    [SerializeField]
    private int weaponType;          //Type of weapon for the animator reference
                                     // -1 - No weapon
                                     // 0  - Melee weapon
                                     // 1  - Ranged weapon
                                     // 2  - Magic Weapon                           
    [SerializeField]
    private float[] itemStat;        //Amout to modify stats by when used or equipped
                                     // - 0 – Health
                                     // - 1 – Stamina
                                     // - 2 – Mana
                                     // - 3 – Strength
                                     // - 4 – Dexterity
                                     // - 5 - Intelligence
                                     // - 6 - Melee Attack
                                     // - 7 - Ranged Attack
                                     // - 8 - Magic Attack
                                     // - 9 - Physical Defense
                                     // - 10 - Magical Defense 

    private int equippedInvNum = -1; // Inventory number of the equipped item

    // Combat vars
    private bool canDamage = false;  //If the item is currently able to do damage when onTriggerEnter is fired, set to true when the player hits attack and false once activated
    private float maxDam = 0.0f;     //Maximum potential damage on hit (player stat values + weapon damage)

    // Projectile to use when shooting or casting, if any
    [SerializeField]
    private Rigidbody projectileEffect;

    //*******************************************************************************************************
    // void OnTriggerEnter(Collider col)
    //   Collider col: Collider of the object entering the trigger
    //
    // Called when something enters the trigger area
    // Checks for valid target and calls HitTarget(GameObject target, int wType), clears canDamage flag
    //*******************************************************************************************************
    void OnTriggerEnter(Collider col)
    {
        // ERROR CHECKING
        //Make sure weapon is active and damage ready
        if (!pInfo.pInv.Inventory[equippedInvNum].GetComponent<ItemDetails>().CanDamage) return;
        //Check for valid target
        if (col.tag != "Enemy") return;
        //Clear canDamage flag so it doesn't continue hitting
        pInfo.pInv.Inventory[equippedInvNum].GetComponent<ItemDetails>().CanDamage = false;

        //If the enemy isn't dead already give damage, otherwise return
        EnemyManager eMan = col.gameObject.GetComponent<EnemyManager>();
        if (eMan.IsDead) return;
        else
        {
            //Call HitPhysical(float maxDam) on the enemy
            eMan.DamageEnemy(0, pInfo.pInv.Inventory[equippedInvNum].GetComponent<ItemDetails>().MaxDamage);

            //Give the player experience for hitting a valid target
            //Strength
            pInfo.pXP.GiveXP(0, 3, 2.0f);
            //Dexterety
            pInfo.pXP.GiveXP(0, 4, 2.0f);
        }
    }
    // END: OnTriggerEnter(Collider col) ********************************************************************

    //*******************************************************************************************************
    // PROPERTIES
    //*******************************************************************************************************
    public string Name
    {
        get
        {
            return itemName;
        }
    }
    public Sprite Icon
    {
        get
        {
            return itemIcon;
        }
    }
    public int Cost
    {
        get
        {
            return itemCost;
        }
    }
    public int ItemType
    {
        get
        {
            return itemType;
        }
    }
    public int WeaponType
    {
        get
        {
            return weaponType;
        }
    }
    public float[] Stats
    {
        get
        {
            return itemStat;
        }
    }
    public bool CanDamage
    {
        get
        {
            return canDamage;
        }
        set
        {
            canDamage = value;
        }
    }
    public int EquippedInvNumber
    {
        get
        {
            return equippedInvNum;
        }
        set
        {
            equippedInvNum = value;
        }
    }
    public Rigidbody Projectile
    {
        get
        {
            return projectileEffect;
        }
    }
    public float MaxDamage
    {
        get
        {
            return maxDam;
        }
        set
        {
            maxDam = value;
        }
    }
}
