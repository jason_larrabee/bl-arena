﻿using UnityEngine;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour {
    //*******************************************************************************************************************************
    // GUIManager.cs
    //
    // Handles all GUI interaction and display of information
    //*******************************************************************************************************************************

    //Player reference
    public Player_Manager pInfo;

    // -- Crosshairs --//
    public GetTarget crosshairs;
    public GameObject[] crosshairDisplay;
    public Text[] crosshairText;

    // -- Message text --
    public Text messageText;


    // - Vital stats panel - //
    [Header("VITALS PANEL")]
    ////Vital HUD Sliders
    public Slider[] vitalSlider;
    ////Vital HUD Text
    public Text[] vitalText;

    // - Hotkeys - //
    [Header("HOTKEYS BAR")]
    public GameObject hotKeyPanel;
    public Button[] hotKeys;            //Reference to all the hotkey buttons
    public Image[] hotKey_Icons;        //All hotkey icons
    public int[] hotKey_Type;           //Sets the type of hotkey
                                        // 0: Item
                                        // 1: Skill
    public int[] hotKey_Ref;            //Reference to inventory slot if item
                                        //Reference to skill array slot if skill
    public Image hotKey_Cursor;         //Displays image of icon by cursor
    [SerializeField]
    private Sprite HOTKEY_DEFAULT_SPRITE;//Default empty hotkey icon

    // - Character Stats Panel - //
    [Header("CHARACTER STATS PANEL")]
    public GameObject characterPanel;
    public Text[] statDisplay;

    // -- Skill Panel -- //
    [Header("CHARACTER SKILLS PANEL")]
    [SerializeField]
    private GameObject skillPanel;		//skill panel 
    [SerializeField]
    private Text text_SkillData;        //Skills information: Name, cost, damage, targetting type
    [SerializeField]
    private Text text_SkillDescription; //Description of the skill
    [SerializeField]
    private Text[] skillName;			//Skill name
    [SerializeField]
    private Text[] skillLevel;          //Skill levels
    [SerializeField]
    private Image[] skillIcon;			//Skills icon
    [SerializeField]
    private Button button_Unlearn;      //Unlearn skill button
    [SerializeField]
    private Scrollbar scrollbar_Skills;
    private int skNum;                  //Number of skill being worked with

    // -- Inventory Panel -- //
    [Header("CHARACTER INVENTORY PANEL")]
    public GameObject inventoryPanel;           //Inventory panel 
    public GameObject itemDetailPanel;     	    //detail panel 
    public Text itemName;						//detail panel item name
    public Text itemStats;	    				//detail panel item stat
    public Text itemCost;						//detail panel item cost
    public Image itemIcon;					    //detail panel item icon
    public GameObject equipPanel;
    public Text[] equipTxt; 					//Equipped item display
    public GameObject usePanel;
    public Sprite emptySpace;					//Empty space graphic
    public Image[] iIcon;						//Items icons on display panel
    public Text goldText;						//Displays the amount of gold the user has
    private int iNum;                           //Inventory number of the item being detailed

    // -- Record Keeper Panel -- //
    public GameObject[] rkPanel;
    public Text dialogueText;

    // -- Guard Panels -- //
    public GameObject guardPanel;
    public GameObject exitGuardPanel;

    // -- Merchant Panel -- //
    [Header("MERCHANT WINDOW")]
    public Merchant mInv;						//Merchants inventory
    public GameObject merchantPanel;
    public ItemDetails mItemDetails;			//Item details - Reused for each item
    public GameObject[] mItemPanel;	        	//Panel containing each inventory item
    public Text pGold;						    //Players gold
    public Text merchantName;					//Name of the merchant
    public Text[] mItemNameText;				//Name of the item
    public Image[] mItemIcon;					//Icon array 
    public Text[] mItemStat;					//Item stats array
    public Text[] mItemCost;					//Item cost array
    private GameObject merchant;
    [SerializeField]
    private Scrollbar scrollbar_MerchantWindow;       //The slider object

    // -- Openable Container Panel -- //
    [Header("CONTAINER WINDOW")]
    [SerializeField]
    GameObject containerPanel;
    [SerializeField]
    Text text_CName;
    [SerializeField]
    Button[] cSlot;
   
    // -- Death Panel -- //
    public GameObject deathPanel;

    // -- Exit Game Panels -- //
    public GameObject exitGamePanel;

    // -- Tutorial HUD -- //
    public TutorialManager tMan;
    public GameObject tutorialPanel;
    public Text tutorial;
    public string[] tutText;

    // FLAGS
    public bool isHotkeying = false;
    public bool isHotkeyingSkill = false;

    //HUD Timers
    private float messageTimer = -1.0f;

    // Use this for initialization
    void Start ()
    {
        //Make our GUI persistant through the levels
        DontDestroyOnLoad(this.gameObject);

        //Disable all information panels
        characterPanel.SetActive(false);
        rkPanel[0].SetActive(false);
        rkPanel[1].SetActive(false);
        rkPanel[2].SetActive(false);
        skillPanel.SetActive(false);
        guardPanel.SetActive(false);
        exitGuardPanel.SetActive(false);
        merchantPanel.SetActive(false);
        containerPanel.SetActive(false);
        inventoryPanel.SetActive(false);
        deathPanel.SetActive(false);
        exitGamePanel.SetActive(false);
        tutorialPanel.SetActive(false);

        //Disable extra hud elements
        crosshairDisplay[1].SetActive(false);
        crosshairText[0].enabled = false;
        crosshairText[1].enabled = false;
        hotKey_Cursor.enabled = false;
        
        //If new game, show tutorial
        if (pInfo.IsNewGame)
        {
            tMan.ShowTutorial(pInfo.TutorialLevel);
            pInfo.TutorialLevel += 1;
        }
    }

    void FixedUpdate()
    {
        UpdateCrosshairs();
        UpdateTimers();
        //If the player is hotkeying an item/skill, keep an icon of the skill next to the pointer
        if (isHotkeying)
            UpdateHotKeyCursor();
        else
        {
            //If not hotkeying and the icon is still enabled, disable it
            if (hotKey_Cursor.enabled) hotKey_Cursor.enabled = false;

        }
    }

    //*******************************************************************************************
    // void UpdateCrosshairs()
    //
    // Keeps the targetting crosshairs up to date
    //*******************************************************************************************
    private void UpdateCrosshairs()
    {
        if (crosshairs.Target != null)
        {
            //If the object is targettable
            if (crosshairs.Target.tag == "Item" || crosshairs.Target.tag == "Guard" || crosshairs.Target.tag == "Merchant" || crosshairs.Target.tag == "Enemy" || crosshairs.Target.tag == "NPC" || crosshairs.Target.tag == "RecordKeeper" || crosshairs.Target.tag == "Door" || crosshairs.Target.tag == "OpenableContainer" )
            {
                //If it is within maximum range, change the reticule
                if (crosshairs.Distance <= 30.0f)
                {
                    crosshairDisplay[0].SetActive(false);
                    crosshairDisplay[1].SetActive(true);
                    //If it is within detail range, show the name
                    if (crosshairs.Distance <= 10.0f)
                    {
                        DisplayName();
                        crosshairText[0].enabled = true;

                    }
                    //Otherwise do not show it
                    else
                    {
                        crosshairText[0].enabled = false;
                        crosshairText[1].enabled = false;

                    }
                }
                //Otherwise show basic crosshairs
                else
                {
                    crosshairDisplay[0].SetActive(true);
                    crosshairDisplay[1].SetActive(false);
                    crosshairText[0].enabled = false;
                    crosshairText[1].enabled = false;
                }
            }
            //Else show basic crosshairs
            else
            {
                crosshairDisplay[0].SetActive(true);
                crosshairDisplay[1].SetActive(false);
                crosshairText[0].enabled = false;
                crosshairText[1].enabled = false;
            }
        }
    }
    // END: void UpdateCrosshairs() *************************************************************

    //*******************************************************************************************
    // void DisplayName()
    //  Called By: FixedUpdate() when valid target is in display range
    //
    // Displays the name of the acquired target in a color representative
    //  of they type of target for easy identification
    //*******************************************************************************************
    private void DisplayName()
    {
        //Gets and sets the name of the targeted object as well as other display parametesrs
        //If targeted object is an item
        if (crosshairs.Target.tag == "Item")
        {
            crosshairText[0].color = Color.blue;
            crosshairText[0].text = crosshairs.Target.GetComponent<ItemDetails>().Name;
        }
        //If targeted item is an enemy
        else if (crosshairs.Target.tag == "Enemy")
        {
            crosshairText[0].color = Color.red;
            crosshairText[0].text = crosshairs.Target.gameObject.GetComponent<EnemyManager>().Name;
        }
        //If targeted item is a door
        else if (crosshairs.Target.tag == "Door")
        {
            crosshairText[0].color = Color.gray;
            crosshairText[0].text = crosshairs.Target.name;
            crosshairText[1].text = "Press 'R' To Open";
            crosshairText[1].enabled = true;
        }
        //If targeted item is an interactive npc
        else if (crosshairs.Target.tag == "Merchant")
        {
            crosshairText[0].color = Color.green;
            crosshairText[0].text = crosshairs.Target.name;
            crosshairText[1].text = "Press 'R' To Shop";
            crosshairText[1].enabled = true;
        }
        else if (crosshairs.Target.tag == "Guard")
        {
            crosshairText[0].color = Color.yellow;
            crosshairText[0].text = crosshairs.Target.name;
            crosshairText[1].text = "Press 'R' To Talk";
            crosshairText[1].enabled = true;
        }
        else if (crosshairs.Target.tag == "LobbyNPC")
        {
            crosshairText[0].color = Color.cyan;
            crosshairText[0].text = crosshairs.Target.GetComponent<LobbyNav>().NPCName;
        }
        else if (crosshairs.Target.tag == "RecordKeeper")
        {
            crosshairText[0].color = Color.magenta;
            crosshairText[0].text = crosshairs.Target.name;
            crosshairText[1].text = "Press 'R' To Talk";
            crosshairText[1].enabled = true;
        }
        else if (crosshairs.Target.tag == "OpenableContainer")
        {
            crosshairText[0].color = Color.yellow;
            crosshairText[0].text = crosshairs.Target.GetComponent<TreasureChest>().Name;
            crosshairText[1].text = "Press 'R' to Open";
            crosshairText[1].enabled = true;
        }
    }
    // END: void DisplayName() ******************************************************************

    private void HideMessage()
    {
        //Hides the game message display
        messageText.enabled = false;
        messageTimer = -1.0f;
    }

    private void UpdateTimers()
    {
        if (messageTimer >= 0.0f)
        {
            messageTimer += Time.deltaTime;
            if (messageTimer >= 5.0f)
            {
                HideMessage();
            }
        }
    }

    public void EnableMerchantItem(int i)
    {
        //Enables the requested item pane, on the merchant
        mItemPanel[i].SetActive(true);
        mItemDetails = mInv.inv[i].GetComponent<ItemDetails>();
        //Set detail data
        string detail = "";
        if (mItemDetails.Stats[0] > 0)
        {
            if (mItemDetails.ItemType == 3 || mItemDetails.ItemType == 4)
            {
                detail = detail + "Health Gain: " + mItemDetails.Stats[0] + "\n";
            }
            else
            {
                detail = detail + "Health: " + mItemDetails.Stats[0] + "\n";
            }
        }
        if (mItemDetails.Stats[1] > 0)
        {
            if (mItemDetails.ItemType == 3 || mItemDetails.ItemType == 4)
            {
                detail = detail + "Stamina Gain: " + mItemDetails.Stats[1] + "\n";
            }
            else
            {
                detail = detail + "Stamina: " + mItemDetails.Stats[1] + "\n";
            }
        }
        if (mItemDetails.Stats[2] > 0)
        {
            if (mItemDetails.ItemType == 3 || mItemDetails.ItemType == 4)
            {
                detail = detail + "Mana Gain: " + mItemDetails.Stats[2] + "\n";
            }
            else
            {
                detail = detail + "Mana: " + mItemDetails.Stats[2] + "\n";
            }
        }
        if (mItemDetails.Stats[3] > 0)
        {
            detail = detail + "Strength:  " + mItemDetails.Stats[3] + "\n";
        }
        if (mItemDetails.Stats[4] > 0)
        {
            detail = detail + "Dexterety:  " + mItemDetails.Stats[4] + "\n";
        }
        if (mItemDetails.Stats[5] > 0)
        {
            detail = detail + "Intelligence:  " + mItemDetails.Stats[5] + "\n";
        }
        if (mItemDetails.Stats[6] > 0)
        {
            detail = detail + "Damage (P):  " + mItemDetails.Stats[6] + "\n";
        }
        if (mItemDetails.Stats[7] > 0)
        {
            detail = detail + "Damage (P):  " + mItemDetails.Stats[7] + "\n";
        }
        if (mItemDetails.Stats[8] > 0)
        {
            detail = detail + "Damage (M):  " + mItemDetails.Stats[8] + "\n";
        }
        if (mItemDetails.Stats[9] > 0)
        {
            detail = detail + "Defense (P):  " + mItemDetails.Stats[9] + "\n";
        }
        if (mItemDetails.Stats[10] > 0)
        {
            detail = detail + "Defense (M):  " + mItemDetails.Stats[10];
        }

        mItemStat[i].text = detail;

        merchantName.text = pInfo.gMan.crosshairs.Target.name.ToString();
        mItemNameText[i].text = mItemDetails.Name.ToString();
        mItemIcon[i].sprite = mItemDetails.Icon;
        mItemCost[i].text = mItemDetails.Cost.ToString() + " G";
    }

    private void DisableMerchantItem(int i)
    {
        //Disables blank items
        mItemPanel[i].SetActive(false);
    }

    //*******************************************************************************************
    // void UpdateVitalPanel()
    //
    // Updates the players vital stats panel
    //*******************************************************************************************
    public void UpdateVitalPanel()
    {
        //Update our sliders
        float vPercent = 0.0f;
        vPercent = Mathf.Round((pInfo.pStats.Health / pInfo.pStats.MaxHealth) * 100.0f);
        vitalSlider[0].value = vPercent;
        vPercent = Mathf.Round((pInfo.pStats.Stamina / pInfo.pStats.MaxStamina) * 100.0f);
        vitalSlider[1].value = vPercent;
        vPercent = Mathf.Round((pInfo.pStats.Mana / pInfo.pStats.MaxMana) * 100.0f);
        vitalSlider[2].value = vPercent;

    	//Update our text display
        vitalText[0].text = Mathf.Round(pInfo.pStats.Health).ToString() + "/" + Mathf.Round(pInfo.pStats.MaxHealth).ToString();
        vitalText[1].text = Mathf.Round(pInfo.pStats.Stamina).ToString() + "/" + Mathf.Round(pInfo.pStats.MaxStamina).ToString();
        vitalText[2].text = Mathf.Round(pInfo.pStats.Mana).ToString() + "/" + Mathf.Round(pInfo.pStats.MaxMana).ToString();
    }
    // END: UpdateVitalPanel() ******************************************************************

    //*******************************************************************************************
    // void UpdateCharPanel()
    //
    // Updates the character panel with the most recent information
    //*******************************************************************************************
    public void UpdateCharPanel()
    {
        //Updates the Character panel with current info
        statDisplay[3].text = Mathf.Round(pInfo.pStats.Strength).ToString();
        statDisplay[4].text = Mathf.Round(pInfo.pStats.Dexterity).ToString();
        statDisplay[5].text = Mathf.Round(pInfo.pStats.Intelligence).ToString();
        statDisplay[6].text = Mathf.Round(pInfo.pStats.MeleeAttack).ToString();
        statDisplay[7].text = Mathf.Round(pInfo.pStats.RangedAttack).ToString();
        statDisplay[8].text = Mathf.Round(pInfo.pStats.MagicAttack).ToString();
        statDisplay[9].text = Mathf.Round(pInfo.pStats.PhysicalDefense).ToString();
        statDisplay[10].text = Mathf.Round(pInfo.pStats.MagicDefense).ToString();
        statDisplay[11].text = "Wins: " + pInfo.pStats.Wins.ToString() + "\nLosses: " + pInfo.pStats.Losses.ToString();
    }
    // END: void UpdateCharPanel() **************************************************************

    public void ShowMessage(string s)
    {
        //Sets the game message text to s and enables the display
        messageText.text = s;
        messageText.enabled = true;
        messageTimer = 0.0f;
    }

    //*****************************************************************************************
    // UpdateSkillPanel()
    //
    //Updates the skill panel with the most recent information
    //*****************************************************************************************
    public void UpdateSkillPanel()
    {
        //Clear detailed information
        text_SkillData.text = "Power: 0\nCost: 0/0/0\nTarget: none";
        text_SkillDescription.text = "none";
        button_Unlearn.enabled = false;

        //Cycle through each skill slot
        for (int i = 0; i < pInfo.pSkills.Skills.Length; i++)
        {
            //If there is a skill

            if (pInfo.pSkills.Skills[i] == null)
            {
                skillName[i].text = "none";
                skillLevel[i].text = "Level: 0";
                skillIcon[i].sprite = HOTKEY_DEFAULT_SPRITE;
            }
            else
            { 
                //Get the skills information
                ItemDetails iInfo = pInfo.pSkills.Skills[i].GetComponent<ItemDetails>();
                //Display the skills name and level
                skillName[i].text = iInfo.Name;
                skillLevel[i].text = "Level: " + pInfo.pSkills.SkillLevel[i];
                //Display the icon
                skillIcon[i].sprite = iInfo.Icon;

            }
 
        }
    }
    
    //*******************************************************************************************
    // public void DisplaySkillInfo(int skillNum)
    //  int skillNum: player skill number to display
    //
    // Displays the skills detailed information in the right side of the skills window
    //*******************************************************************************************
    public void DisplaySkillInfo(int skillNum)
    {
        if(pInfo.pSkills.Skills[skillNum] == null)
        {
            text_SkillData.text = "Power: 0\nCost: 0/0/0\nTarget: none";
            text_SkillDescription.text = "none";
            button_Unlearn.enabled = false;

        }
        //Set the skill info
        else
        {
            skNum = skillNum;
            SkillDetails skInfo = pInfo.pSkills.Skills[skillNum].GetComponent<SkillDetails>();
            //Set target string base of targetting flags
            string tgt = "Other";
            if (skInfo.Projectile)
            {
                tgt = "Projectile";
            }
            if (skInfo.Other)
            {
                tgt = "Other";
            }
            if (skInfo.Self)
            {
                tgt = "Self";
            }
            //Get the skills power
            float pwr = 0;
            ItemDetails iInfo = pInfo.pSkills.Skills[skillNum].GetComponent<ItemDetails>();
            float skLevel = pInfo.pSkills.SkillLevel[skillNum];
            for (int i = 0; i < iInfo.Stats.Length; i++)
            {
                if (iInfo.Stats[i] > 0) pwr = iInfo.Stats[i];
                //Multiply by skill level modifier
                pwr *= skLevel; 
            }
            //Set the data
            text_SkillData.text = "Power: " + pwr + "\nCost: " + (skInfo.Cost.x * skLevel) + "/" + (skInfo.Cost.y * skLevel) + "/" + (skInfo.Cost.z * skLevel) + "\nTarget: " + tgt;
            //Set description
            text_SkillDescription.text = skInfo.Description;
            //Enable Unlearn button
            button_Unlearn.enabled = true;
        }
    }
    // END: void DisplaySkillInfo(int skillNum) *************************************************

    public void UnLearnSkill()
    {
        //Remove any set hotkey
        for (int i = 0; i < hotKeys.Length; i++)
        {
            if (hotKey_Ref[i] == skNum && hotKey_Type[i] == 1)
            {
                hotKey_Ref[i] = -1;
                hotKey_Type[i] = -1;
                hotKey_Icons[i].sprite = HOTKEY_DEFAULT_SPRITE;
                break;
            }
        }
        //Call UnLearnSkill() on the player
        pInfo.pSkills.UnLearnSkill(skNum);
    }
    //*******************************************************************************************
    // public void BuyItem(int slot)
    //  int slot: merchants item slot containing the item
    //
    // Purchases an item from a merchant
    //*******************************************************************************************
    public void BuyItem(int slot)
    {
 
        //Get the item
        GameObject mItem = merchant.GetComponent<Merchant>().inv[slot];
        //Get the item details
        ItemDetails mItemInfo = mItem.GetComponent<ItemDetails>();
        //Make sure payer has enough gold
        if (pInfo.pInv.Gold >= mItemInfo.Cost)
        {
            //Attempt to add the item to inventory
            if (!pInfo.pInv.AddItem(mItem))
            {
                //If the player doesn't have room, send message to player through the GUI manager
                ShowMessage("Purchase failed. Not enough room in inventory.");
            }
            else
            {
                //Take the player's money
                pInfo.pInv.Gold -= mItemInfo.Cost;
                
                pGold.text = pInfo.pInv.Gold.ToString();
                goldText.text = pGold.text;

                //Sed success message to player
                ShowMessage("You have purchased the " + mItemInfo.Name);
            }
        }
        //Not enough money
        else
        {
            //Send not enough money message to GUI MANAGER for player
            ShowMessage("You do not have enough gold to buy that item");
        }
    }
    // END: void BuyItem(int slot) **************************************************************

    //*******************************************************************************************
    // public void LootItemFromContainer(int num)
    //  int num: which slot to loot from
    //
    // Removes the selected item from the container and places it in inventory
    //*******************************************************************************************
    public void LootItemFromContainer(int num)
    {
        TreasureChest tc = crosshairs.Target.GetComponentInParent<TreasureChest>();
        if (tc.Contents[num] != null)
        {
            //Attempt to add the item to inventory
            if (!pInfo.pInv.AddItem(tc.Contents[num]))
            {
                //If the player doesn't have room, send message to player through the GUI manager
                ShowMessage("Purchase failed. Not enough room in inventory.");
            }
            else
            {
                //Send success message to player
                ShowMessage("You have obtained the " + tc.Contents[num].GetComponent<ItemDetails>().Name);
                //Remove the item from the chest
                tc.Contents[num] = null;
                cSlot[num].image.sprite = HOTKEY_DEFAULT_SPRITE;
                tc.NumItems--;
                if (tc.NumItems <= 0)
                {
                    ToggleContainerPanel();
                    tc.Empty();
                }
            }
        }
    }
    // END: void LootItemFromContainer() ********************************************************

    //*******************************************************************************************
    // public void UpdateInventoryPanel()
    //
    // Update the inventory display panel
    //*******************************************************************************************
    public void UpdateInventoryPanel()
    {
        //Loop through our inventory slots resetting any empty slots to their default image
        for (int i = 0; i < pInfo.pInv.Inventory.Length; i++)
        {
            if (pInfo.pInv.Inventory[i] == null)
            {
                iIcon[i].sprite = emptySpace;
            }
            else
            {
                iIcon[i].sprite = pInfo.pInv.iInfo[i].Icon;
            }
        }
        //Update the gold display text
        goldText.text = pInfo.pInv.Gold.ToString() + " Gold";
    }
    // END: void UpdateInventoryPanel() *********************************************************

    //*******************************************************************************************
    // void DetailInvItem(int n) 
    //  int n: slot of the inventory item to get the information of
    //
    // Sets the display details for the requested inventory item
    //*******************************************************************************************
    public void DetailInvItem(int n)
    {
        if (pInfo.pInv.Inventory[n] != null)
        {
            //Set iNum for button use
            iNum = n;

            //Check for ctrl+click
            if (Input.GetButton("CtrlClick"))
            {
                //Shortcut past the detail part and directly interact
                InteractItem();
            }
            //IF alt-clicking and the item is a potion, use the potion instead of interacting
            else if (Input.GetButton("AltClick") && pInfo.pInv.iInfo[n].ItemType == 3)
            {
                pInfo.pInv.UseItem(n);
                CloseItemDetail();
            }
            else
            {
                //Set the information
                itemName.text = pInfo.pInv.iInfo[n].Name;

                //Set detail data
                string detail = "";
                if (pInfo.pInv.iInfo[n].Stats[0] > 0)
                {
                    if (pInfo.pInv.iInfo[n].ItemType == 3 || pInfo.pInv.iInfo[n].ItemType == 4)
                    {
                        detail = detail + "Health Gain: " + pInfo.pInv.iInfo[n].Stats[0] + "\n";
                    }
                    else
                    {
                        detail = detail + "Health: " + pInfo.pInv.iInfo[n].Stats[0] + "\n";
                    }
                }
                if (pInfo.pInv.iInfo[n].Stats[1] > 0)
                {
                    if (pInfo.pInv.iInfo[n].ItemType == 3 || pInfo.pInv.iInfo[n].ItemType == 4)
                    {
                        detail = detail + "Stamina Gain: " + pInfo.pInv.iInfo[n].Stats[1] + "\n";
                    }
                    else
                    {
                        detail = detail + "Stamina: " + pInfo.pInv.iInfo[n].Stats[1] + "\n";
                    }
                }
                if (pInfo.pInv.iInfo[n].Stats[2] > 0)
                {
                    if (pInfo.pInv.iInfo[n].ItemType == 3 || pInfo.pInv.iInfo[n].ItemType == 4)
                    {
                        detail = detail + "Mana Gain: " + pInfo.pInv.iInfo[n].Stats[2] + "\n";
                    }
                    else
                    {
                        detail = detail + "Mana: " + pInfo.pInv.iInfo[n].Stats[2] + "\n";
                    }
                }
                if (pInfo.pInv.iInfo[n].Stats[3] > 0)
                {
                    detail = detail + "Strength:  " + pInfo.pInv.iInfo[n].Stats[3] + "\n";
                }
                if (pInfo.pInv.iInfo[n].Stats[4] > 0)
                {
                    detail = detail + "Dexterety:  " + pInfo.pInv.iInfo[n].Stats[4] + "\n";
                }
                if (pInfo.pInv.iInfo[n].Stats[5] > 0)
                {
                    detail = detail + "Intelligence:  " + pInfo.pInv.iInfo[n].Stats[5] + "\n";
                }
                if (pInfo.pInv.iInfo[n].Stats[6] > 0)
                {
                    detail = detail + "Damage (P):  " + pInfo.pInv.iInfo[n].Stats[6] + "\n";
                }
                if (pInfo.pInv.iInfo[n].Stats[7] > 0)
                {
                    detail = detail + "Damage (P):  " + pInfo.pInv.iInfo[n].Stats[7] + "\n";
                }
                if (pInfo.pInv.iInfo[n].Stats[8] > 0)
                {
                    detail = detail + "Damage (M):  " + pInfo.pInv.iInfo[n].Stats[8] + "\n";
                }
                if (pInfo.pInv.iInfo[n].Stats[9] > 0)
                {
                    detail = detail + "Defense (P):  " + pInfo.pInv.iInfo[n].Stats[9] + "\n";
                }
                if (pInfo.pInv.iInfo[n].Stats[10] > 0)
                {
                    detail = detail + "Defense (M):  " + pInfo.pInv.iInfo[n].Stats[10];
                }

                itemStats.text = detail;
                itemCost.text = pInfo.pInv.iInfo[n].Cost.ToString() + " Gold";
                itemIcon.sprite = pInfo.pInv.iInfo[n].Icon;

                //Enable the detail panel
                itemDetailPanel.SetActive(true);
            }
        }
        else
            return;
    }
    // END: void DisplayInvItem(int n) **********************************************************

    //*******************************************************************************************
    // void InteractItem()
    //
    // Directs the item to the proper interaction based on item type when the player clicks
    //  on the items icon in the details window
    //*******************************************************************************************
    public void InteractItem()
    {
        //Set the item type
        int iType = pInfo.pInv.iInfo[iNum].ItemType;
        //No item found so return
        if (iType == -1) return;
        //Perform action based of iType
        if (iType < 3)
        {
            // 0 - 2: Equip the item
            //If item is equipped, unequip it
            if (pInfo.pInv.Equipped[iType] == iNum)
            {
                pInfo.pInv.UnEquipItem(iNum);
            }
            //Otherwise equip it
            else
            {
                pInfo.pInv.EquipItem(iNum);
            }

        }
        else if (iType == 3)
        {
            //Potion
            HotkeyItem();
        }
        else if (iType == 4)
        {
            //Skill
            pInfo.pSkills.LearnSkill(pInfo.pInv.Inventory[iNum]);
            //Remove the item
            pInfo.pInv.RemoveItem(iNum);
            //Close detail panel
            CloseItemDetail();
        }
        else return;
    }
    // END: void InteractItem() *****************************************************************

    //*******************************************************************************************
    // void DisplayEquipped(int e)
    //  int e: inventory slot of the item being equipped
    //
    // Sets the display to equipped
    //*******************************************************************************************
    public void DisplayEquipped(int e)
    {
        //Displayed the equipped symbol on an equipped item
        if (!equipTxt[e].enabled)
        {
            equipTxt[e].text = "E";
            equipTxt[e].enabled = true;
        }
        else
        {
            equipTxt[e].enabled = false;
        }
    }
    // END: void DisplayEquipped(int e)**********************************************************
    
    //*******************************************************************************************
    // void HotkeyItem()
    //
    // Sets the hotkey flags to begin the hotkeying process
    //*******************************************************************************************
    public void HotkeyItem()
    {
        //Make sure its a valid item
        if (pInfo.pInv.iInfo[iNum].ItemType == 3)
        {
            //Set flags
            isHotkeying = true;
            isHotkeyingSkill = false;
        }
    }
    // END: void HotkeyItem() *******************************************************
    //*******************************************************************************************
    // void HotkeySkill(int skillNum)
    //
    // Sets the hotkey flags to begin the hotkeying process
    //*******************************************************************************************
    public void HotkeySkill(int skillNum)
    {
        //Make sure its a valid item
        if (pInfo.pSkills.Skills[skillNum] != null)
        {
            //Set flags
            skNum = skillNum;
            isHotkeying = true;
            isHotkeyingSkill = true;
        }
    }
    // END: void HotkeyItem() *******************************************************

    //*******************************************************************************************
    // void UpdateHotKeyCursor()
    //
    // Makes sure the icon of what is being hotkeys is displayed next to the cursor
    //*******************************************************************************************
    private void UpdateHotKeyCursor()
    {
        //Make sure icon is enabled
        if (!hotKey_Cursor.enabled) hotKey_Cursor.enabled = true;
        //Set its position next to standard cursor
        hotKey_Cursor.transform.position = new Vector3(Input.mousePosition.x - 32.0f, Input.mousePosition.y - 32.0f, Input.mousePosition.z - 32.0f);
        //Set the icon to the item/skill icon
        // - Item
        if (!isHotkeyingSkill)
        {
            hotKey_Cursor.sprite = pInfo.pInv.iInfo[iNum].Icon;
        }
        // - Skill
        else
        {
            hotKey_Cursor.sprite = pInfo.pSkills.Skills[skNum].GetComponent<ItemDetails>().Icon;
        }
    }
    // END: void UpdateHotKeyCursor() ***********************************************************

    //***********************************************************************
    // void UseHotKey(int key)
    //  int key: hotkey being pressed
    // Called when a hot key is pressed - calls the appropriate action into
    //  play
    //***********************************************************************
    public void UseHotkey(int key)
    {
        //Check if we are setting a hotkey rather than using it
        if (isHotkeying)
        {
            // - Skill
            if (isHotkeyingSkill)
            {
                //Set the graphic
                hotKey_Icons[key].sprite = pInfo.pSkills.Skills[skNum].GetComponent<ItemDetails>().Icon;
                //Set the details
                hotKey_Type[key] = 1;
                hotKey_Ref[key] = skNum;
                pInfo.pSkills.HotKeyed[skNum] = key;
                //Set the flags
                isHotkeying = false;
                isHotkeyingSkill = false;
                //Cleanup
                hotKey_Cursor.enabled = false;
                HotKeyClearDuplicates(true, skNum, key);
            }
            // - Item
            else
            {
                //Set the graphic
                hotKey_Icons[key].sprite = pInfo.pInv.iInfo[iNum].Icon;
                //Set the details
                hotKey_Type[key] = 0;
                hotKey_Ref[key] = iNum;
                //Set the flags
                isHotkeying = false;
                isHotkeyingSkill = false;
                //Cleanup
                hotKey_Cursor.enabled = false;
                HotKeyClearDuplicates(false, iNum, key);
            }
            return;
        }
        // Using hotkey
        else
        {
            // - Use item
            if (hotKey_Type[key] == 0)
            {
                //Set iNum for inventory use
                iNum = hotKey_Ref[key];
                //Use the item
                pInfo.pInv.UseItem(iNum);

                //Clean up
                hotKey_Type[key] = -1;
                hotKey_Ref[key] = -1;
                hotKey_Icons[key].sprite = HOTKEY_DEFAULT_SPRITE;
                iNum = -1;
                return;
            }
            // - Use skill
            else
            {

                return;
            }
        }
    }
    // END: void UseHotKey(int key) *****************************************

    //*******************************************************************************************
    // void HotKeyClearDuplicates(bool isSkill, int num, int key)
    //  bool isSkill: true to check skills, false for items
    //  int num: item/skill number to check for
    //  int key: hotkey number that was just set
    //
    // Cycles through the hotkeys and removes duplicates when hotkeying
    //*******************************************************************************************
    private void HotKeyClearDuplicates(bool isSkill, int num, int key)
    {
        for (int i = 0; i < hotKey_Ref.Length; i++)
        {
            if (isSkill)
            {
                //Don't check against just set key
                if (i != key)
                {
                    //If we have a match, remove it
                    if (hotKey_Ref[i] == num && hotKey_Type[i] == 1)
                    {
                        hotKey_Ref[i] = -1;
                        hotKey_Type[i] = -1;
                        hotKey_Icons[i].sprite = HOTKEY_DEFAULT_SPRITE;
                    }
                }
            }
            else
            {
                //Don't check against just set key
                if (i != key)
                {
                    //If we have a match, remove it
                    if (hotKey_Ref[i] == num && hotKey_Type[i] == 0)
                    {
                        hotKey_Ref[i] = -1;
                        hotKey_Type[i] = -1;
                        hotKey_Icons[i].sprite = HOTKEY_DEFAULT_SPRITE;
                    }
                }
            }
        }
    }
    // END: void HotKeyClearDuplicates(bool isSkill, int num, int key) **************************

    //*******************************************************************************************
    // void HotkeyClearUsedItem(int invNum)
    //  int invNum: inventory slot of the item used
    //
    // Cycles through the hotkeys and clears a hotkeyed item that was used from inventory
    //*******************************************************************************************
    public void HotkeyClearUsedItem(int invNum)
    {
        for (int i = 0; i < hotKey_Ref.Length; i++)
        {
            //If found, perform cleanup
            if (hotKey_Ref[i] == invNum)
            {
                //Clear the key settings
                hotKey_Ref[i] = -1;
                hotKey_Type[i] = -1;
                hotKey_Icons[i].sprite = HOTKEY_DEFAULT_SPRITE;
                return;
            }
        }
    }
    // END: void HotkeyClearUsedItem(int invNum) ************************************************

 
    //******************************************************************************************************************
    // PANEL DISPLAY TOGGLES
    //
    // Toggles respective panels open and closed
    //  Populates any necessary fields when opening or cleans them up before closing
    //******************************************************************************************************************

    //*******************************************************************************************
    // void ToggleCharPanel()
    //*******************************************************************************************
    public void ToggleCharPanel()
    {
        //If not enabled, enable it
        if (!characterPanel.activeSelf)
        {
            //UpdateCharPanel();
            characterPanel.SetActive(true);
        }
        //Else disable it
        else
        {
            characterPanel.SetActive(false);
        }
    }
    // END: ToggleCharPanel() *******************************************************************

    //*******************************************************************************************
    // void ToggleRecordKeeperPanel()
    //*******************************************************************************************
    public void ToggleRecordKeeperPanel()
    {
        if (!rkPanel[0].activeSelf)
        {
            rkPanel[0].SetActive(true);
            RecordKeeper rk = GameObject.FindGameObjectWithTag("RecordKeeper").GetComponent<RecordKeeper>();
            if (pInfo.pStats.Wins >= rk.winsNeeded)
            {
                dialogueText.text = rk.dialogue[1];
                rkPanel[1].SetActive(true);
                rkPanel[2].SetActive(false);
            }
            else
            {
                dialogueText.text = rk.dialogue[0];
                rkPanel[1].SetActive(false);
                rkPanel[2].SetActive(true);
            }
        }
        else
        {
            rkPanel[0].SetActive(false);
        }

    }
    // END: ToggleRecordKeeperPanel() ***********************************************************

    //*******************************************************************************************
    // void ToggleSkillPanel()
    //*******************************************************************************************
    public void ToggleSkillPanel()
    {
        //Toggle skill panel canvas on and off
        if (!skillPanel.activeSelf)
        {
            //Update the information
            UpdateSkillPanel();
            //Enable the display
            skillPanel.SetActive(true);
            scrollbar_Skills.value = 1.0f;
        }
        else
        {
            //Disable the display
            skillPanel.SetActive(false);
        }
    }
    // END: void ToggleSkillPanel() *************************************************************
    
    //*******************************************************************************************
    // void ToggleGuardPanel()
    //*******************************************************************************************
    public void ToggleGuardPanel()
    {
        //Shows the arena guard interaction menu
        if (!guardPanel.activeSelf)
        {
            guardPanel.SetActive(true);
        }
        else
        {
            guardPanel.SetActive(false);
        }
    }
    // END: void ToggleGuardPanel() *************************************************************

    //*******************************************************************************************
    // void ToggleExitGuardPanel()
    //*******************************************************************************************
    public void ToggleExitGuardPanel()
    {
        //Shows the arena guard interaction menu
        if (exitGuardPanel.activeSelf)
        {
            exitGuardPanel.SetActive(false);
        }
        else
        {
            exitGuardPanel.SetActive(true);
        }
    }
    // END: void ToggleExitGuardPanel() *********************************************************

    //*******************************************************************************************
    // void ToggleInventoryPanel()
    //*******************************************************************************************
    public void ToggleInventoryPanel()
    {
        //If not enabled, enable it
        if (!inventoryPanel.activeSelf)
        {
            inventoryPanel.SetActive(true);
            itemDetailPanel.SetActive(false);
        }
        //Else disble it
        else
        {
            inventoryPanel.SetActive(false);
            itemDetailPanel.SetActive(false);
        }
    }
    // END: void ToggleInventoryPanel() *********************************************************

    //*******************************************************************************************
    // void ToggleMerchantPanel()
    //*******************************************************************************************
    public void ToggleMerchantPanel()
    {
        if (!merchantPanel.activeSelf)
        {
            //Set the merchant
            merchant = pInfo.gMan.crosshairs.Target;
            //Get Our merchant script
            mInv = merchant.GetComponent<Merchant>();
            //Get players gold
            pGold.text = pInfo.pInv.Gold.ToString();
            //Cycle through merchants inventory
            for (int i = 0; i < 10; i++)
            {
                //Populate all item slots
                if (mInv.inv[i] != null)
                {
                    EnableMerchantItem(i);
                }
                //Disable any empty item slots
                else
                {
                    DisableMerchantItem(i);
                }
            }

            //Enable the panel
            merchantPanel.SetActive(true);
            scrollbar_MerchantWindow.value = 1.0f;
        }
        else
        {
            merchant = null;
            merchantPanel.SetActive(false);
            for (int m = 0; m < 10; m++)
            {
                DisableMerchantItem(m);
            }
        }
    }
    // END: ToggleMerchantPanel() ***************************************************************

    //*******************************************************************************************
    // void ToggleContainerPanel
    //*******************************************************************************************
    public void ToggleContainerPanel()
    {
        if (!containerPanel.activeSelf)
        {
            TreasureChest tc = crosshairs.Target.GetComponentInParent<TreasureChest>();
            GameObject[] contents = tc.Contents;
            text_CName.text = tc.Name;
            for (int i = 0; i < cSlot.Length; i++)
            {
                if (contents[i] != null)
                {
                    ItemDetails info = contents[i].GetComponent<ItemDetails>();
                    cSlot[i].image.sprite = info.Icon;
                }
            } 
            containerPanel.SetActive(true);
        }
        else
        {
            TreasureChest tc = crosshairs.Target.GetComponentInParent<TreasureChest>();
            tc.Empty();
            for (int i = 0; i < cSlot.Length; i++)
            {
                cSlot[i].image.sprite = HOTKEY_DEFAULT_SPRITE;
            }
            containerPanel.SetActive(false);
        }
    }
    // END: ToggleContainerPanel()***************************************************************

    //*******************************************************************************************
    // void CloseItemDetail() 
    //*******************************************************************************************
    public void CloseItemDetail()
    {
        itemDetailPanel.SetActive(false);
        iNum = -1;
    }
    // END: CloseItemDetail() *******************************************************************

    //*******************************************************************************************
    // void ToggleDeathPanel()
    //*******************************************************************************************
    public void ToggleDeathPanel()
    {
        //Opens and closes the death panel that shows when the player dies
        if (!deathPanel.activeSelf)
        {
            deathPanel.SetActive(true);
        }
        else
        {
            deathPanel.SetActive(false);
        }
    }
    // END: void ToggleDeathPanel() *************************************************************

    //*******************************************************************************************
    // void ToggleExitGamePanel()
    //*******************************************************************************************
    public void ToggleExitGamePanel()
    {
        //Opens and closes the exit game options
        if (!exitGamePanel.activeSelf)
        {
            exitGamePanel.SetActive(true);
        }
        else
        {
            exitGamePanel.SetActive(false);
        }
    }
    // END: void ToggleExitGamePanel() **********************************************************

    //*******************************************************************************************
    // void ToggleTutorialPanel()
    //*******************************************************************************************
    public void ToggleTutorialPanel()
    {
        //Toggles tutorial canvas on and off
        if (!tutorialPanel.activeSelf)
        {
            tutorialPanel.SetActive(true);
        }
        else
        {
            tutorialPanel.SetActive(false);
            //if (pInfo.pStats.tutLevel == 8)
            //{
              //  EndTutorial();
            //}
        }
    }
    // END: void ShowTutorialPanel() ************************************************************

    //*******************************************************************************************
    // PROPERTIES
    //*******************************************************************************************
    public int INum
    {
        get
        {
            return iNum;
        }
    }

    public GameObject Skills
    {
        get
        {
            return skillPanel;
        }
    }

    public GameObject ContainerPanel
    {
        get
        {
            return containerPanel;
        }
    }
}
