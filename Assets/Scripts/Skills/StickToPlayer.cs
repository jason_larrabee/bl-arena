﻿using UnityEngine;


public class StickToPlayer : MonoBehaviour {
    //****************************************************************************************
    // StickToPlayer.cs
    //
    // Script to make a spell effect stick to the player's position as he moves around
    //****************************************************************************************

    //The player
    private GameObject playerChar;

    //******************************************************************
    // void Awake()
    //
    // Called when the skill is cast, finds the player
    //******************************************************************
    void Awake()
    {
        this.playerChar = GameObject.FindGameObjectWithTag("Player");
    }
    // END: void Awake() ***********************************************

    //******************************************************************
    // void Update()
    //
    // Called once per frame, keeps the skill effect centered on target
    //******************************************************************
    void Update()
    {
        this.transform.position = this.playerChar.transform.position;
        this.transform.rotation = this.playerChar.transform.rotation;
        Destroy(this.gameObject, 3.0f);
    }
    // END: void Update() **********************************************
}
