﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenu : MonoBehaviour {
    //************************************************************************
    //  mainMenu.cs
    //
    //  Main Menu script for the game.  Displays "cinematic" camera panning 
    //   around the scene and handles main menu input
    //************************************************************************

   // [SerializeField]
   // private GameObject camRotator;          //Game object camera is bound to, rotate to create camera circling arena effect

	//************************************************************************
    // void Update()
    //
    // Called once per frame.  Only used to make camera rotate
    //************************************************************************
	//void Update () {
        //Rotates the camera around the central rotator
	//    camRotator.transform.Rotate(Vector3.up, 5f * Time.deltaTime);
	//}

    //************************************************************************
    // void StartGame()
    // Called By: Start Button on main menu
    //
    // Starts the game
    //************************************************************************
    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    //************************************************************************
    // void QuitGame()
    // Called By: Quit Button on main menu
    //
    // Quits the game
    //************************************************************************
    public void QuitGame()
    {
        Application.Quit();
    }
}