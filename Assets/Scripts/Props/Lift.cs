﻿using UnityEngine;


public class Lift : MonoBehaviour {

    public GameObject LiftObject;
    public GameObject Lever;        //Lever that controls the lift
    public Animator lAnim;
 
    public float liftHeight;
    public Transform liftBase;

    private float leverTimer = 0.0f;

    private bool isDown = true;
    private bool inAction = false;

    void FixedUpdate()
    {
        UpdateTimers();
        if (inAction)
            MoveElevator();
    }

    public void PullLever()
    {

        lAnim.SetBool("isPulled", true);
        leverTimer += Time.deltaTime;
        
    }



    private void UpdateTimers()
    {
        if (leverTimer > 0.0f)
        {
            leverTimer += Time.deltaTime;
            if (leverTimer > 0.5f)
            {
                lAnim.SetBool("isPulled", false);
                leverTimer = 0.0f;
                MoveElevator();
            }
        }
    }

    private void MoveElevator()
    {
        if (isDown)
        {
            inAction = true;
            this.transform.localPosition += new Vector3(0.0f, 0.1f, 0.0f);
            if (this.transform.localPosition.y >= liftHeight)
            {
                this.transform.localPosition = new Vector3(this.transform.localPosition.x, liftHeight, this.transform.localPosition.z);
                inAction = false;
                isDown = false;
            }
        }
        else if (!isDown)
        {
            inAction = true;
            this.transform.localPosition -= new Vector3(0.0f, 0.1f, 0.0f);
            GameObject.FindGameObjectWithTag("Player").transform.position -= new Vector3(0.0f, 0.1f, 0.0f);
            if (this.transform.localPosition.y <= liftBase.localPosition.y)
            {
                this.transform.localPosition = new Vector3(this.transform.localPosition.x, liftBase.localPosition.y, this.transform.localPosition.z);
                inAction = false;
                isDown = true;
            }
        }
    }

}
