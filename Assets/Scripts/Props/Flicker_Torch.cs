﻿using UnityEngine;
using System.Collections;

public class Flicker_Torch : MonoBehaviour {
    //********************************************************************************************
    //  Flicker_Torch.cs
    //
    //  Gives a torch a flickering light effect
    //********************************************************************************************

    //The actual light to flicker
    [SerializeField]
    private Light flicker;

    //Light intensity settings - Flickers between light intensities
    [SerializeField]
    private float maxIntensity;             //Maximum light intensity
    [SerializeField]
    private float minIntensity;             //Minimum light intensity
    [SerializeField]
    private float newIntensity;             //Target light intensity
    [SerializeField]
    private float lastIntensity;            //Last light intensity

    //Color settings - Flickers between these colors
    [SerializeField]
    private Color colorOne;                 //color 1 - Set in inspector
    [SerializeField]
    private Color colorTwo;                 //color 2 - Set in inspector
    [SerializeField]
    private Color colorThree;               //color 3 - Assigned by ColorFlicker()
    [SerializeField]
    private Color colorFour;                //color 4 - Assigned by ColorFlicker()

    //Timers
    private float flickTimer = 0.00f;       //Time between flickers
	
    //*******************************************************************************
    // void Start()
    //
    // Initializes the lighting settings
    //*******************************************************************************
	void Start () {
	    //Get the light source
        this.flicker = this.GetComponent<Light>();
	}
    // END: void Start() ************************************************************
	
	//*******************************************************************************
    // void Update()
    //
    // Tracks and maintains flick timer
    //*******************************************************************************
	void Update () {
	    //Increment flickTimer
        this.flickTimer += Time.deltaTime;
        //If the timer is up, update the light and reset timer
        if (this.flickTimer >= 0.10f)
        {
            this.ColorFlicker();
            this.UpdateLight();
            this.flickTimer = 0.00f;
        }
	}
    // END: void Update() ***********************************************************

    //*******************************************************************************
    // void UpdateLight()
    // Called By: Update() when timer is up
    //
    // Modifies the lighting settings based on ColorFlicker() results
    //*******************************************************************************
    private void UpdateLight()
    {
        //Get a new random intensity between max and minimum range
        this.newIntensity = Random.Range(this.minIntensity, this.maxIntensity);
        //Lerp to our new intensity
        this.flicker.intensity = Mathf.Lerp(this.lastIntensity, this.newIntensity, Time.smoothDeltaTime);
        //Set the new intensity as the last intensity for the next pass
		this.lastIntensity = this.newIntensity;
		//Lerp to our new colors
        this.flicker.color = Color.Lerp(this.colorThree, this.colorFour, Time.smoothDeltaTime);
	
    }
    // END: void UpdateLight() ******************************************************

    //*******************************************************************************
    // void ColorFlicker()
    // Called By: Update function
    //
    // Randomly selects light colors to flick to, colors set in the inspector
    //*******************************************************************************
    private void ColorFlicker()
    {
        int i = Random.Range(0, 3);
        switch (i)
        {
            case 0:
                this.colorThree = this.colorOne;
                this.colorFour = this.colorTwo;
                break;
            case 1:
                this.colorThree = this.colorTwo;
                this.colorFour = this.colorOne;
                break;
            case 3:
                this.colorThree = this.colorOne;
                this.colorFour = this.colorOne;
                break;
            case 4:
                this.colorThree = this.colorTwo;
                this.colorFour = this.colorTwo;
                break;
        }
    }
}
