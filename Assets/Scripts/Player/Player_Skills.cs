﻿using UnityEngine;

public class Player_Skills : MonoBehaviour {

    //Player Manager
    [SerializeField]
    private Player_Manager pInfo;

    // - Active Skills
    private GameObject[] skills;
    private int[] skillLevels;
    private int[] skillHotkeyed;

    // - Passive skills
    //REGENERATION SKILLS
    private float[] regenSkills;
    // - 0 - Basic regen
    // - 1 - Health Regen
    // - 2 - Stamina Regen
    // - 3 - Mana Regen

    //Timers
    private float regenTimer = 0.0f;          

    //******************************************************************************************
    // void Start()
    //
    // Initializes the player's skills.
    // Gets all player script references.
    //******************************************************************************************
    void Start () {

        //Initialize skills
        // - If its a new game, start with base stats
        if (pInfo.IsNewGame)
        {
            //Limit the player to 20 active skills
            skills = new GameObject[20];
            skillLevels = new int[20];
            skillHotkeyed = new int[20];
            for (int i = 0; i < skillHotkeyed.Length; i++)
            {
                skillHotkeyed[i] = -1;
            }

            //Set regen values to archetype bases
            switch (pInfo.Archetype)
            {
                case 0:
                    regenSkills = new float[4] { 1, 2, 1, 1 };
                    break;
                case 1:
                    regenSkills = new float[4] { 1, 1, 2, 1 };
                    break;
                case 2:
                    regenSkills = new float[4] { 1, 1, 1, 3 };
                    break;
            }
        }
        // - If loading saved game, get values from save file
        else
        {
            
        }
	}
    // END: void Start() ***********************************************************************

    // Update is called once per frame
    void FixedUpdate()
    {
        //Regenerate lost vitals
        Regenerate();
    }

    //******************************************************************************************
    // void Regenerate()
    //
    // Regenerates lost vital stats
    //******************************************************************************************
    private void Regenerate()
    {
        //Increment counter
        regenTimer += Time.deltaTime;
        //Regen every second
        if (regenTimer >= 1.0f)
        {
            if (pInfo.pStats.Health < pInfo.pStats.MaxHealth)
            {
                pInfo.pStats.ModifyVital(0, (regenSkills[0] + regenSkills[1]));
                //Give experience to the used skills
                pInfo.pXP.GiveXP(1, 0, regenSkills[0]);
                pInfo.pXP.GiveXP(1, 1, regenSkills[1]);
            }
            if (pInfo.pStats.Stamina < pInfo.pStats.MaxStamina && !pInfo.pMC.IsSprinting)
            {
                pInfo.pStats.ModifyVital(2, (regenSkills[0] + regenSkills[2]));
                //Give experience to the used skills
                pInfo.pXP.GiveXP(1, 0, regenSkills[0]);
                pInfo.pXP.GiveXP(1, 2, regenSkills[2]);
            }
            if (pInfo.pStats.Mana < pInfo.pStats.MaxMana)
            {
                pInfo.pStats.ModifyVital(4, (regenSkills[0] + regenSkills[3]));
                //Give experience to the used skills
                pInfo.pXP.GiveXP(1, 0, regenSkills[0]);
                pInfo.pXP.GiveXP(1, 3, regenSkills[3]);
            }
            //Reset the timer
            regenTimer = 0.0f;
        }
    }
    // END: void Regenerate() ******************************************************************

    //*********************************************************************************************
    // PUBLIC ACCESS
    //  void LearnSkill(GameObject skill)
    //  void RaiseSkillLevel(int skillNumber)
    //  void CastSkill(int skillNumber)
    //  Accessors
    //*********************************************************************************************
    //******************************************************************************************
    // public void LearnSkill(GameObject skill)
    //  GameObject skill : skill to learn
    //
    // If there is room, teaches the player a new skill.  Otherwise gives an error
    //  message
    //******************************************************************************************
    public void LearnSkill(GameObject skill)
    {
        //Cycle through the skill slots looking for the first opening
        for (var i = 0; i < 20; i++)
        {
            //Once found, set it the new skill and return
            if (skills[i] == null)
            {
                skills[i] = skill;
                skillLevels[i] = 1;
                pInfo.gMan.ShowMessage("You have learned the skill: " + skill.name);
                //Update the skill panel
                pInfo.gMan.UpdateSkillPanel();
                return;
            }
            //If the skill book is full, let the player know and return
            else if (i == 19 && skills[i] != null)
            {
                pInfo.gMan.ShowMessage("You cannot learn any more skills.");
                return;
            }
        }
    }
    // END: void LearnSkill(GameObject skill) **************************************************

    //******************************************************************************************
    // public void UnLearnSkill(int skillNum)
    //  int skillNum: number of skill in skill array to unlearn
    //
    // Unlearns requested skill
    //******************************************************************************************
    public void UnLearnSkill(int skillNum)
    {
        //Clear the skill
        skills[skillNum] = null;
        skillLevels[skillNum] = 0;

        //Cycle through the remaining skills moving them forward one
        for (var i = skillNum; i < skills.Length; i++)
        {
            if (i != skills.Length - 1)
            {
                skills[i] = skills[i + 1];
                skillLevels[i] = skillLevels[i + 1];
                skillHotkeyed[i] = skillHotkeyed[i + 1];
                if(skillHotkeyed[i] > -1)
                {
                    pInfo.gMan.hotKey_Ref[skillHotkeyed[i]] = i;
                }
            }
        }
        //Update skill panel
        pInfo.gMan.UpdateSkillPanel();
    }
    // END: void LearnSkill(GameObject skill) **************************************************
    //******************************************************************************************
    // public void RaiseSkillLevel(int skillNumber)
    //  int skillNumber: number of the skill to raise
    //
    // Raises the requested skills level by one
    //******************************************************************************************
    public void RaiseSkillLevel(int skillNumber)
    {
        skillLevels[skillNumber] += 1;
        pInfo.gMan.ShowMessage(skills[skillNumber].name + " increased to level " + skillLevels[skillNumber] + "!");

    }
    // END: RaiseSkillLevel(int skillNumber) ***************************************************

    //******************************************************************************************
    // public void RaiseRegenLevel(int regenNumber)
    //  int regenNumber: number of the regen skill to raise
    //
    // Raises the requested regen level by one
    //******************************************************************************************
    public void RaiseRegenLevel(int regenNumber)
    {
        regenSkills[regenNumber] += 1;
        if (regenNumber == 0) pInfo.gMan.ShowMessage("Base Regeneration increased!");
        if (regenNumber == 1) pInfo.gMan.ShowMessage("Health Regeneration increased!");
        if (regenNumber == 2) pInfo.gMan.ShowMessage("Stamina Regeneration increased!");
        if (regenNumber == 3) pInfo.gMan.ShowMessage("Mana Regeneration increased!");

    }
    // END: void RaiseRegenLevel(int regenNumber) **********************************************

    //******************************************************************************************
    // public void CastSkill(int skillNumber)
    //  int skillNumber: number of the skill to cast
    //
    // Casts the requested skill
    //******************************************************************************************
    public void CastSkill(int skillNumber)
    {
        //Get the skills information
       // SkillDetails sInfo = skills[skillNumber].GetComponent<SkillDetails>();
        //Check for enough vital to cast
        //if (pInfo.pStats.Health > sInfo.skillCost.x && pInfo.pStats.Stamina >= sInfo.skillCost.y && pInfo.pStats.Mana >= sInfo.skillCost.z)
        //{
            //Call on the skills Cast(int skillLevel) method
            //sInfo.Cast(skillLevels[skillNumber]);
        //}
       // else
        //{
            //Give unable to cast message
        //}
    }
    // END: CastSkill(int skillNumber) *********************************************************

    //******************************************************************************************
    // ACCESSORS
    //******************************************************************************************
    public int[] SkillLevel
    {
        get
        {
            return skillLevels;
        }
    }
    public float[] RegenLevel
    {
        get
        {
            return regenSkills;
        }
    }

    public GameObject[] Skills
    {
        get
        {
            return skills;
        }
    }
    public int[] HotKeyed
    {
        get
        {
            return skillHotkeyed;
        }
        set
        {
            skillHotkeyed = value;
        }
    }

    //FOR REFERENCE ONLY - REMOVE WHEN FINISHED
    //public void UseSkill(int s)
    //{
    //    //Takes the skill information and sends it off to the skill handler to handle the rest
    //    iInfo = pInfo.pSkills.playerSkills[s].GetComponent<ItemDetails>();
    //    sInfo = pInfo.pSkills.playerSkills[s].GetComponent<SkillDetails>();
    //    if (sInfo.skillType == "Magic" && castingTimer >= 1)
    //    {
    //        if (pInfo.pVitals.Mana >= (sInfo.skillCost.z * pInfo.pXP.skillLevel[s]))
    //        {
    //            pInfo.pVitals.Mana -= (sInfo.skillCost.z * pInfo.pXP.skillLevel[s]);
    //            sInfo.Cast(s);
    //            //Set flags
    //            pInfo.pCom.isCasting = true;
    //            pInfo.pCom.canAttack = false;
    //            pInfo.pCom.canShoot = false;
    //            pInfo.pCom.canCast = false;
    //            pInfo.pCom.canDefend = false;
    //            pInfo.pCom.hitTimer = 0.0f;
    //            pInfo.pCom.updateHitTimer = true;
    //            //Give casting XP
    //            pInfo.pXP.GiveSkillXP(s, 2);
    //            //Give Attribute XP
    //            pInfo.pXP.GivePrimaryXP(2, 2);
    //            pInfo.pXP.GiveSecondaryXP(2, 2);
    //            //Set recast timer
    //            castingTimer = 0;

    //        }
    //    }
    //    if (sInfo.skillType == "Melee" && castingTimer >= 1)
    //    {
    //        if (pInfo.pVitals.Stamina >= (sInfo.skillCost.y * pInfo.pXP.skillLevel[s]))
    //        {
    //            pInfo.pVitals.Stamina -= (sInfo.skillCost.y * pInfo.pXP.skillLevel[s]);
    //            sInfo.Cast(s);
    //            //Set flags
    //            pInfo.pCom.isAttacking = true;
    //            pInfo.pCom.canAttack = false;
    //            pInfo.pCom.canShoot = false;
    //            pInfo.pCom.canCast = false;
    //            pInfo.pCom.canDefend = false;
    //            pInfo.pCom.hitTimer = 0.0f;
    //            pInfo.pCom.updateHitTimer = true;
    //            //Give casting XP
    //            pInfo.pXP.GiveSkillXP(s, 2);
    //            //Give attribute xp
    //            pInfo.pXP.GivePrimaryXP(0, 2);
    //            pInfo.pXP.GivePrimaryXP(1, 2);
    //            pInfo.pXP.GiveSecondaryXP(0, 2);
    //        }
    //    }
    //    if (sInfo.skillType == "Ranged" && castingTimer >= 1)
    //    {
    //        if (pInfo.pVitals.Stamina >= (sInfo.skillCost.y * pInfo.pXP.skillLevel[s]))
    //        {
    //            pInfo.pVitals.Stamina -= (sInfo.skillCost.y * pInfo.pXP.skillLevel[s]);
    //            sInfo.Cast(s);
    //            //Set flags
    //            pInfo.pCom.isShooting = true;
    //            pInfo.pCom.canAttack = false;
    //            pInfo.pCom.canShoot = false;
    //            pInfo.pCom.canCast = false;
    //            pInfo.pCom.canDefend = false;
    //            pInfo.pCom.hitTimer = 0.0f;
    //            pInfo.pCom.updateHitTimer = true;

    //            //Give casting XP
    //            pInfo.pXP.GiveSkillXP(s, 2);
    //            //Give attribute xp
    //            pInfo.pXP.GivePrimaryXP(0, 2);
    //            pInfo.pXP.GivePrimaryXP(1, 2);
    //            pInfo.pXP.GiveSecondaryXP(1, 2);
    //        }
    //    }
    //}
}
