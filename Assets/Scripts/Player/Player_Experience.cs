﻿using UnityEngine;


public class Player_Experience : MonoBehaviour {
    //****************************************************************************************
    // Player_Experience.cs
    //
    // Handles the player's experience levels
    //  Adds XP to a stat/skill
    //  Ranks the stat/skill up when it reaches the next level
    //****************************************************************************************
    //External script references
    private Player_Manager pInfo;             //Player Manager script

    //Stat Experience Levels
    private float[] statXP;
    //Vital XPs
    // - 0 - MaxHealth
    // - 1 - MaxStamina
    // - 2 - MaxMana
    //Primary XPs
    // - 3 – Strength
    // - 4 – Dexterity
    // - 5 - Intelligence
    //Secondary XPs
    // - 6 - Melee Attack
    // - 7 - Ranged Attack
    // - 8 - Magic Attack
    // - 9 - Physical Defense
    // - 10 - Magical Defense 
    private float[] stat_NextLevel;         //Amount of experience needed for the next stat level
                                            //Vitals = Current level * 5;
                                            //All others = Current level * 100

    //Skills
    // - Passive
    //Regeneration skills XP
    private float[] regenXP;                
    // - 0 - Base regeneration
    // - 1 - Health regeneration
    // - 2 - Stamina regeneration
    // - 3 - Mana regeneration 
    private float[] regen_NextLevel;        //Amount of experience needed for the next regen level
                                            //Current level * 200
    // - Active
    private float[] skillsXP;               //Skills XP
    private float[] skills_NextLevel;       //Amount of experience needed for the next skill level
                                            //Current level * 500


    //******************************************************************************************
    // void Start()
    //
    // Initializes the player's experience levels
    // Gets all player script references.
    //******************************************************************************************
    void Start ()
    {
        //Get reference to player manager
        pInfo = GetComponent<Player_Manager>();

        //Init xp arrays
        // New game
        if (pInfo.IsNewGame)
        {
            statXP = new float[11];
            stat_NextLevel = new float[11];
            regenXP = new float[4];
            regen_NextLevel = new float[4];
            skillsXP = new float[20];
            skills_NextLevel = new float[20];
        }
        // Load from file
        else
        {

        }
	}
    // END: void Start() ***********************************************************************
	
    //******************************************************************************************
    // private void CalculateNextLevel(int xpType)
    //  int xpType: experience pool to calculate
    //
    // Calculates the amount of experience needed for the next level
    //******************************************************************************************
    private void CalculateNextLevel(int xpType)
    {
        switch (xpType)
        {
            case 0: //Stats
                //Vitals
                stat_NextLevel[0] = pInfo.pStats.MaxHealth * 5.0f;
                stat_NextLevel[1] = pInfo.pStats.MaxStamina * 5.0f;
                stat_NextLevel[2] = pInfo.pStats.MaxMana * 5.0f;
                //Primaries
                stat_NextLevel[3] = pInfo.pStats.Strength * 50.0f;
                stat_NextLevel[4] = pInfo.pStats.Dexterity * 50.0f;
                stat_NextLevel[5] = pInfo.pStats.Intelligence * 50.0f;
                //Secondaries
                stat_NextLevel[6] = pInfo.pStats.MeleeAttack * 100.0f;
                stat_NextLevel[7] = pInfo.pStats.RangedAttack * 100.0f;
                stat_NextLevel[8] = pInfo.pStats.MagicAttack * 100.0f;
                stat_NextLevel[9] = pInfo.pStats.PhysicalDefense * 100.0f;
                stat_NextLevel[10] = pInfo.pStats.MagicDefense * 100.0f;
                break;

            case 1: //Regen
                regen_NextLevel[0] = pInfo.pSkills.RegenLevel[0] * 250.0f;
                regen_NextLevel[1] = pInfo.pSkills.RegenLevel[1] * 250.0f;
                regen_NextLevel[2] = pInfo.pSkills.RegenLevel[2] * 250.0f;
                regen_NextLevel[3] = pInfo.pSkills.RegenLevel[3] * 250.0f;
                break;

            case 2: //Skills
                for (int i = 0; i < 20; i++)
                {
                    skills_NextLevel[i] = pInfo.pSkills.SkillLevel[i] * 500.0f;
                }
                break;
        }
    }
    // END: CalculateNextLevel() ***************************************************************

    //******************************************************************************************
    // public void GiveXP(int xpType, int arrayPos, float amount)
    //  int xpType: experience pool to give to
    //  int arrayPos: position in the array of the stat
    //  float amount: amount of xp to give
    //
    // Gives the requested experience to the requested stat
    // Calls raise skill/stat and calculates new xp requirements as needed
    //******************************************************************************************
    public void GiveXP(int xpType, int arrayPos, float amount)
    {
        switch (xpType)
        {
            case 0: //Stats
                statXP[arrayPos] += amount;
                //Check if level up
                if (statXP[arrayPos] >= stat_NextLevel[arrayPos])
                {
                    //Roll over extra xp
                    statXP[arrayPos] -= stat_NextLevel[arrayPos];
                    //Raise the stat
                    // - vitals
                    if (arrayPos == 0)
                    {
                        pInfo.pStats.IncreaseStat(0, 1, 1.0f);
                    }
                    if (arrayPos == 1)
                    {
                        pInfo.pStats.IncreaseStat(0, 3, 1.0f);
                    }
                    if (arrayPos == 2)
                    {
                        pInfo.pStats.IncreaseStat(0, 5, 1.0f);
                    }
                    // - primaries
                    else if (arrayPos >= 3 && arrayPos < 6)
                    {
                        pInfo.pStats.IncreaseStat(1, arrayPos - 3, 1.0f);
                    }
                    // - secondaries
                    else if (arrayPos >= 6)
                    {
                        pInfo.pStats.IncreaseStat(2, arrayPos - 6, 1.0f);
                    }
                    //Calculate new next level amount
                    CalculateNextLevel(xpType);
                }
                break;

            case 1: //Regen
                regenXP[arrayPos] += amount;
                //Check if level up
                if (regenXP[arrayPos] >= regen_NextLevel[arrayPos])
                {
                    //Roll over extra xp
                    regenXP[arrayPos] -= regen_NextLevel[arrayPos];
                    //Raise the stat
                    pInfo.pSkills.RaiseRegenLevel(arrayPos);
                    //Calculate new next level amount
                    CalculateNextLevel(xpType);
                }
                break;

            case 2: //Skills
                skillsXP[arrayPos] += amount;
                //Check if level up
                if (skillsXP[arrayPos] >= skills_NextLevel[arrayPos])
                {
                    //Roll over extra xp
                    skillsXP[arrayPos] -= skills_NextLevel[arrayPos];
                    //Raise the skill level
                    pInfo.pSkills.RaiseSkillLevel(arrayPos);
                    //Calculate new next level amount
                    CalculateNextLevel(xpType);
                }
                break;
        }
    }
}
