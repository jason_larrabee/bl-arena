﻿using UnityEngine;


public class EnemyInventory : MonoBehaviour {
    //************************************************************************************************
    // EnemyInventory.cs
    //
    // As the enemies technically have no inventory, this script is used to set dropped loot when 
    //  killed, if any.
    //************************************************************************************************
    [SerializeField]
    private GameObject[] itemDrops;         //Array of all dropped items on death
    [SerializeField]
    private int[] dropAmount;               //Array of quantity of each item to potentially drop
    [SerializeField]
    private int goldDrop;                   //Amount of gold the enemy will potentially drop
                                            //Random amount between goldDrop and goldDrop/2

    //************************************************************************************************
    // PROPERTIES
    //************************************************************************************************
    public GameObject[] ItemDrops
    {
        get
        {
            return itemDrops;
        }
    }
    public int[] DropAmount
    {
        get
        {
            return dropAmount;
        }
    }
    public int Gold
    {
        get
        {
            return goldDrop;
        }
    }
   
}
