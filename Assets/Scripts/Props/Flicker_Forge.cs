﻿using UnityEngine;

public class Flicker_Forge : MonoBehaviour {
    //******************************************************************************************
    // 
    [SerializeField]
	private Light flicker;
    [SerializeField]
	private float maxIntensity;
    [SerializeField]
	private float minIntensity;
    [SerializeField]
	private float newIntensity;
    [SerializeField]
	private float lastIntensity;

    [SerializeField]
	private float maxAngle;
    [SerializeField]
	private float minAngle;
    [SerializeField]
	private float newAngle;
    [SerializeField]
	private float lastAngle;

    [SerializeField]
    private Color colorOne;
    [SerializeField]
    private Color colorTwo;
    [SerializeField]
    private Color colorThree;
    [SerializeField]
    private Color colorFour;

    [SerializeField]
    private float flickTimer;

    void Start () {
	    this.flicker = GetComponentInParent<Light>();
	    this.maxIntensity = 2.0f;
	    this.minIntensity = 1.2f;
	    this.lastIntensity = 0.2f;
	
	    this.maxAngle = 170.00f;
	    this.minAngle = 145.00f;
	    this.lastAngle = 145.00f;
    }

    void Update () {
	    flickTimer += Time.deltaTime;
	    CheckTimer();
    }

    void FixedUpdate() {
	    ColorFlicker();
    }

    private void CheckTimer(){
	    if (flickTimer >= 0.15){
		    this.newIntensity = Random.Range(minIntensity, maxIntensity);
		    this.flicker.intensity = Mathf.Lerp(lastIntensity, newIntensity, Time.smoothDeltaTime);
		    this.lastIntensity = this.newIntensity;
		
		    this.newAngle = Random.Range(minAngle, maxAngle);
		    this.flicker.spotAngle = Mathf.LerpAngle(lastAngle, newAngle, Time.smoothDeltaTime);
		    this.lastAngle = this.newAngle;
		
		    this.GetComponent<Light>().color = Color.Lerp(colorThree, colorFour, Time.deltaTime);
		    this.flickTimer = 0;		
	    }
    }

    private void ColorFlicker(){
	    var i = Random.Range(0, 3);
	    switch (i){
		    case 0:
			    this.colorThree = colorOne;
			    this.colorFour = colorTwo;
			    break;
		    case 1:
			    this.colorThree = colorTwo;
			    this.colorFour = colorOne;
			    break;
		    case 3:
			    this.colorThree = colorOne;
			    this.colorFour = colorOne;
			    break;
		    case 4:
			    this.colorThree = colorTwo;
			    this.colorFour = colorTwo;
			    break;
	    }
    }
}
