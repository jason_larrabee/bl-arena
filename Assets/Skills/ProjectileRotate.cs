﻿using UnityEngine;

public class ProjectileRotate : MonoBehaviour {
    //***********************************************************************************
    // ProjectileRotate.cs
    //
    // Rotates a projectile about its axis as it flies through the air
    //***********************************************************************************
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.Rotate(Vector3.up * 8.0f);
        this.transform.Rotate(Vector3.right * 8.0f);
	}
}
