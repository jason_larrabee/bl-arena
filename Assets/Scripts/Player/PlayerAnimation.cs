﻿using UnityEngine;

public class PlayerAnimation : MonoBehaviour {
    //****************************************************************************************
    // PlayerAnimation.cs
    //
    // Ties player animations to animator
    //****************************************************************************************
    Player_Manager pInfo;            //Player Manager script
    [SerializeField]
    Animator anim;                  //Animator on player

    //*******************************************************************************
    // void Start()
    //
    // Initializes the script by setting all references
    //*******************************************************************************
    void Start()
    {
        pInfo = GetComponentInParent<Player_Manager>();
    }
    // END: void Start() ************************************************************

    //*******************************************************************************
    // void Update()
    //
    // Keeps animation flags in animator up to date with current actions
    //*******************************************************************************
    void Update()
    {
        anim.SetFloat("walk", Mathf.Abs(Input.GetAxis("Vertical")));
        //If not moving forward/backward then activate the strafe animation
        if (Input.GetAxis("Vertical") == 0.0f)
            anim.SetFloat("strafe", Input.GetAxis("Horizontal"));
        //Otherwise do not play strafe animation
        else
            anim.SetFloat("strafe", 0.0f);

        anim.SetBool("jumping", pInfo.pMC.IsJumping);
        anim.SetBool("attacking", pInfo.pCom.IsAttacking);
        anim.SetBool("shooting", pInfo.pCom.IsShooting);
        anim.SetBool("casting", pInfo.pCom.IsCasting);
        anim.SetBool("blocking", pInfo.pCom.IsBlocking);


    }
    // END: void Update() ***********************************************************

    public Animator Anim
    {
        get
        {
            return anim;
        }
        set
        {
            anim = value;
        }
    }
}
