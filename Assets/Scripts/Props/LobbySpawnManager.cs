﻿using UnityEngine;

public class LobbySpawnManager : MonoBehaviour {

    [SerializeField]
    private Transform spawnPoint;
    [SerializeField]
    private int maxNPCCount;
    private int curNPCCount;
    [SerializeField]
    private float spawnTime;
    private float spawnTimer;

    [SerializeField]
    private GameObject[] NPCs;

	// Use this for initialization
	void Start () {
        SpawnNPC();
	}
	
	// Update is called once per frame
	void Update () {

        //Increment the spawn timer if its running
        if (spawnTimer > 0.0f)
        {
            spawnTimer += Time.deltaTime;
        }

        if (spawnTimer >= spawnTime)
        {
            SpawnNPC();
        }
	
	}

    //****************************************************************
    // void SpawnNPC()
    //
    // Spawns an NPC from the list of available NPCs in front of the 
    //  door to the arena
    //****************************************************************
    private void SpawnNPC()
    {
        //Pick a random enemy from the list
        int i = Random.Range(0, NPCs.Length);
        //Spawn picked enemy
        Instantiate(NPCs[i], spawnPoint.position, spawnPoint.rotation);
        curNPCCount++;
        //Reset the spawn timer
        ResetTimer();
    }
    // END: void SpawnNPC() ******************************************

    //****************************************************************
    // void StartTimer()
    //
    // Starts the spawn timer after an existing NPC leaves the room
    //****************************************************************
    public void StartTimer()
    {
        spawnTimer += Time.deltaTime;
    }
    // END: void StartTimer() ****************************************

    //****************************************************************
    // void ResetTimer()
    //
    // Resets the spawn timer to 0.0f so it will not tick off until there
    //  is room to spawn another NPC
    //****************************************************************
    private void ResetTimer()
    {
        spawnTimer = 0.0f;
        //If we are still under the maximum allowed number of npcs, start the timer again
        if(curNPCCount < maxNPCCount)
        {
            StartTimer();
        }
    }
    // END: void ResetTimer() ****************************************

    //****************************************************************
    // ACCESSORS
    //****************************************************************
    // - int curNPCCount
    public int CurNPCCount
    {
        set
        {
            curNPCCount = value;
        }
        get
        {
            return curNPCCount;
        }
    }
}
