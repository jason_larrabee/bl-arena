﻿using UnityEngine;


public class StickToEnemy : MonoBehaviour {
    //****************************************************************************************
    // StickToEnemy.cs
    //
    // Script to make a spell effect stick to the enemies position as he moves around
    //****************************************************************************************

    //Enemy char game object
    private GameObject enemyChar;

    //******************************************************************
    // void Awake()
    //
    // Called when the skill is cast, finds the enemy
    //******************************************************************
    void Awake()
    {
        this.enemyChar = GameObject.FindGameObjectWithTag("Enemy");
    }
    // END: void Awake() ***********************************************

    //******************************************************************
    // void Update()
    //
    // Called once per frame, keeps the skill effect centered on target
    //******************************************************************
    void Update()
    {
        if (this.enemyChar != null)
        {
            this.transform.position = this.enemyChar.transform.position;
        }
        Destroy(this.gameObject, 7);
    }
    // END: void Update() **********************************************
}
