﻿using UnityEngine;
using System.Collections;

public class SkillDetails : MonoBehaviour {
    //*********************************************************************************************
    // SKillDetails.cs
    //
    // Holds of a skills information and proper castinga actions
    //*********************************************************************************************

    //Skill effects
    public Rigidbody skillProjectile_r1;			//Rank 1 - Level 1 - 3
    public Rigidbody skillProjectile_r2;           //Rank 2 - Level 4 - 7
    public Rigidbody skillProjectile_r3;           //Rank 3 - Level 8 - 10

    //Skill data
    [SerializeField]
    private string skillSchool;                     //Primary school the skill is from (destruction, restoration, etc..)
    [SerializeField]
    private string[] prerequisiteSkills;            //All prerequisite skills needed to learn this skill
    [SerializeField]
    private int[] prerequisiteLevels;               //Level requirement of prereq skills(1 - know the skills, 2+ required level to learn)

    public string skillType;						//Type of skill(Melee, Ranged, Magic)
    [SerializeField]
    private Vector3 skillCost;						//x = health, y = stamina, z = mana
    [SerializeField]
    private string skillDesc;                        //Skill description
    public Vector3 skillPosition;                   //Position to cast the skill

    //External references
    public GameObject playerChar;                   //Player character
    public Player_Manager pInfo;                     //Player manager
    [SerializeField]
    private ItemDetails iInfo;                       //Item details
    public Hit sInfo;                               //Hit target information

    //Skill flags
    [SerializeField]
    private bool isProjectile;
    [SerializeField]
    private bool isTargetted;
    [SerializeField]
    private bool isSelfTargetted;
    public bool isDot;
    public bool isSpray;

    //Target information
    public GameObject enemyChar;                    //Enemy character
    private EnemyManager eInfo;                     //Enemy manager




    //**********************************************************************
    // void Cast(int s)
    //  int s: skill number of the skill being cast
    //
    // Casts skill s 
    // Determines the type of skill and casts appropriately
    //**********************************************************************
    public void Cast(int s)
    {
        //playerChar = GameObject.FindGameObjectWithTag("Player");
        //pInfo = playerChar.GetComponent<PlayerManager>();

        //enemyChar = GameObject.FindGameObjectWithTag("Enemy");
        //eInfo = enemyChar.GetComponent<EnemyManager>();

        //iInfo = GetComponent<ItemDetails>();
        ////skillLevel = pInfo.pXP.skillLevel[s];
        ////Projectile Skill
        //if (isProjectile)
        //{
        //    Vector3 sPos = playerChar.transform.position;
        //    sPos.x -= 0.5f;
        //    sPos.y += 1.5f;
        //    Rigidbody skillProj = (Rigidbody)Instantiate(skillProjectile_r1, sPos, playerChar.transform.rotation);
        //    skillProj.velocity = skillProj.transform.forward * 30;
        //    sInfo = skillProj.GetComponent<Hit>();
        //    //sInfo.skillNum = s;
        //}
        ////Target-based skill
        //else if (isTargetted)
        //{
        //    if (pInfo.pInput.selected == null)
        //    {
        //        pInfo.pHUD.ShowMessage("Your spell fizzled!");
        //        return;
        //    }
        //    if (pInfo.pInput.selected != null && pInfo.pInput.selectedDistance > 15)
        //    {
        //        pInfo.pHUD.ShowMessage("Your spell fizzled!");
        //        return;
        //    }
        //    int d = 0;
        //    Vector3 tgtPos = enemyChar.transform.position;
        //    tgtPos.y += 1.0f;
        //    Instantiate(skillProjectile_r1, tgtPos, enemyChar.transform.rotation);
        //    //DIRECT DAMAGE
        //    if (iInfo.itemType == 9)
        //    {
        //        d = Random.Range(0, ((iInfo.itemStat * skillLevel) + pInfo.pStats.modMagicAttack + (pInfo.pStats.modIntelligence / 10)) - eInfo.eStats.modMagicDefense);
        //        eInfo.eVitals.Health -= d;
        //        pInfo.pXP.GivePrimaryXP(2, 2);
        //        pInfo.pXP.GiveSecondaryXP(2, d / 5);
        //        pInfo.pXP.GiveSkillXP(s, d / 10);
        //    }
        //    //Damage over Time
        //    if (iInfo.itemType == 10)
        //    {
        //        int max = ((iInfo.itemStat * skillLevel) + pInfo.pStats.modMagicAttack + (pInfo.pStats.modIntelligence / 10) - eInfo.eStats.modMagicDefense);
        //        pInfo.pXP.GivePrimaryXP(2, 2);
        //        pInfo.pXP.GiveSecondaryXP(2, max / 5);
        //        pInfo.pXP.GiveSkillXP(s, max / 10);
        //        eInfo.eVitals.DamageOverTime(max, 7);
        //    }
        //}
        ////Self skill
        //else if (isSelfTargetted)
        //{
        //    int h = 0;
        //    Instantiate(skillProjectile_r1, playerChar.transform.position, playerChar.transform.rotation);
        //    //Health heal
        //    if (iInfo.itemType == 11)
        //    {
        //        h = Random.Range(0, ((iInfo.itemStat * skillLevel) + pInfo.pStats.modMagicAttack + (pInfo.pStats.modIntelligence / 10)));
        //        pInfo.pVitals.Health += h;
        //        //Give success experience
        //        pInfo.pXP.GivePrimaryXP(2, 2);
        //        pInfo.pXP.GiveSecondaryXP(2, h / 5);
        //        pInfo.pXP.GiveSkillXP(s, 3);
        //    }
        //    //Stamina heal
        //    if (iInfo.itemType == 12)
        //    {
        //        h = Random.Range(0, ((iInfo.itemStat * skillLevel) + pInfo.pStats.modMagicAttack + (pInfo.pStats.modIntelligence / 10)));
        //        pInfo.pVitals.Stamina += h;
        //        //Give success experience
        //        pInfo.pXP.GivePrimaryXP(2, 2);
        //        pInfo.pXP.GiveSecondaryXP(2, h / 5);
        //        pInfo.pXP.GiveSkillXP(s, 3);
        //    }
        //    //Mana heal
        //    if (iInfo.itemType == 13)
        //    {
        //        h = Random.Range(0, ((iInfo.itemStat * skillLevel) + pInfo.pStats.modMagicAttack + (pInfo.pStats.modIntelligence / 10)));
        //        pInfo.pVitals.Mana += h;
        //        //Give success experience
        //        pInfo.pXP.GivePrimaryXP(2, 2);
        //        pInfo.pXP.GiveSecondaryXP(2, h / 5);
        //        pInfo.pXP.GiveSkillXP(s, 3);
        //    }
        //    //Attribute buffs
        //    if (iInfo.itemType >= 14 && iInfo.itemType <= 21)
        //    {
        //        h = Random.Range(1, ((iInfo.itemStat * skillLevel) + pInfo.pStats.modMagicAttack + (pInfo.pStats.modIntelligence / 10)));
        //        pInfo.pStats.magicMod[iInfo.itemType - 14] = h / 2;
        //        pInfo.pStats.CalculateModAttributes();
        //        //Give success experience
        //        pInfo.pXP.GivePrimaryXP(2, 2);              //Intelligence gets two experience points for attempting the cast
        //        pInfo.pXP.GiveSecondaryXP(2, h / 4);        //Magic Attack gets the amount of the buff divided by four
        //        pInfo.pXP.GiveSkillXP(s, 3);                //Skill cast gains 3 experience
        //    }
        //}
        //else if (isSpray)
        //{
        //    Instantiate(skillProjectile_r1, playerChar.transform.position, playerChar.transform.rotation);
        //    // - Firespray
        //    if (iInfo.itemType == 10)
        //    {
        //        int max = ((iInfo.itemStat * skillLevel) + pInfo.pStats.modMagicAttack + (pInfo.pStats.modIntelligence / 10) - eInfo.eStats.modMagicDefense);
        //        pInfo.pXP.GivePrimaryXP(2, 2);
        //        pInfo.pXP.GiveSecondaryXP(2, max / 5);
        //        pInfo.pXP.GiveSkillXP(s, max / 10);
        //    }
        //}

    }
    // END: void Cast(int s) ***********************************************

    //**********************************************************************
    // void DestroyProjectile()
    //
    // Destroys the cast projectile
    //**********************************************************************
    public void DestroyProjectile()
    {
        Destroy(this.gameObject, 0);
    }
    // END: void DestroyProjectile() ***************************************


    //**********************************************************************
    // ACCESSORS
    //**********************************************************************
    // - skillSchool
    public string School
    {
        get
        {
            return skillSchool;
        }
    }
    // - prerequisiteSkills
    public string[] PrereqSkills
    {
        get
        {
            return prerequisiteSkills;
        }
    }
    // - prerequisiteLevels
    public int[] PrereqLevels
    {
        get
        {
            return prerequisiteLevels;
        }
    }
    // - skillCost
    public Vector3 Cost
    {
        get
        {
            return skillCost;
        }
    }
    // -skillDesc
    public string Description
    {
        get
        {
            return skillDesc;
        }
    }
    // - iInfo
    public ItemDetails ItemInfo
    {
        get
        {
            return iInfo;
        }
    }
    // - isProjectile
    public bool Projectile
    {
        get
        {
            return isProjectile;
        }
    }
    // - isProjectile
    public bool Other
    {
        get
        {
            return isTargetted;
        }
    }
    // - isProjectile
    public bool Self
    {
        get
        {
            return isSelfTargetted;
        }
    }
}
