﻿using UnityEngine;

public class Merchant : MonoBehaviour {
    //***********************************************************************
    // Merchant.cs
    //
    // Holds the array of merchant items for sale
    // Attach to a merchant NPC and drop item prefabs into the inv array through
    //  the inspector to sell them
    //***********************************************************************

    //Merchants inventory
    public GameObject[] inv;  //Array to hold all the merchants items

   
}
