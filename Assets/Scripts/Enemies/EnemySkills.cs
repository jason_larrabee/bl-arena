﻿using UnityEngine;

public class EnemySkills : MonoBehaviour {
    //*************************************************************************************************************************
    // EnemySkills.cs
    //
    //Enemy skills are handled slightly differently than player skills in that they do not level up over time in Arena
    //  When using this script on larger Badlands projects, skill xp will need to be included so the enemies may level up 
    //  the longer they exist in the game world and perform various actions
    //All skill levels will be set in the editor upon NPC creation
    //Only certain default skills will be directly included in script, the rest will be dropped into the editor as game objects
    //*************************************************************************************************************************
    [SerializeField]
    private EnemyManager eInfo;

    //Passive Skills
    [SerializeField]
    private float[] regen;


    //Timers
    private float regenTimer = 0.0f;


    // Update is called once per frame
    void FixedUpdate()
    {
        //Regenerate lost vitals
        Regenerate();
    }

    //******************************************************************************************
    // void Regenerate()
    //
    // Regenerates lost vital stats
    //******************************************************************************************
    private void Regenerate()
    {
        //Increment counter
        regenTimer += Time.deltaTime;
        //Regen every second
        if (regenTimer >= 1.0f)
        {
            if (eInfo.eStats.Health < eInfo.eStats.MaxHealth)
            {
                eInfo.eStats.ModifyVital(0, (regen[0] + regen[1]));
            }
            if (eInfo.eStats.Stamina < eInfo.eStats.MaxStamina)
            {
                eInfo.eStats.ModifyVital(2, (regen[0] + regen[2]));
            }
            if (eInfo.eStats.Mana < eInfo.eStats.MaxMana)
            {
                eInfo.eStats.ModifyVital(4, (regen[0] + regen[3]));
            }
            //Reset the timer
            regenTimer = 0.0f;
        }
    }


}
