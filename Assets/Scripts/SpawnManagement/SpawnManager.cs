﻿using UnityEngine;


public class SpawnManager : MonoBehaviour {
    //**********************************************************************************
    // SpawnManager.cs
    //
    // Contains the various spawn points for spawning enemies.  
    // Spawns the enemeies and destroys them when the fight is over
    //
    //**********************************************************************************
    [SerializeField]
    GameObject[] SpawnPoints_Player;            //Player SpawnPoints
    [SerializeField]
    GameObject[] SpawnPoints;				    //Enemy spawn points for the arenas
    //Arrays containing the available enemeies to spawn in each arena
    [SerializeField]
    Rigidbody[] Enemies_Tutorial;				//Tutorial
    [SerializeField]
    Rigidbody[] Enemies_One;				    //Arena 1
    [SerializeField]
    Rigidbody[] Enemies_Two;				    //Arena 2
    [SerializeField]
    Rigidbody[] Enemies_Three;				    //Arena 3

    Rigidbody enemy;                        //Reference to the enemy spawned
    Rigidbody player;                       //Reference to player

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>();
    }


    //**********************************************************************************
    // void SpawnNPC(int aNum)
    //  int aNum: arena number to spawn enemy in
    //      0 - Tutorial
    //      1+ - Arena number
    // Spawns an enemy from the enemies list
    //**********************************************************************************
    public void SpawnNPC(int aNum)
    {
        int i = 0;
        //Randomly pick an opponent to spawn from the appropriate list and spawn it
        switch (aNum)
        {
            case 0:
                i = 0;
                enemy = (Rigidbody)Instantiate(Enemies_Tutorial[i], SpawnPoints[aNum].transform.position, SpawnPoints[aNum].transform.rotation);
                break;
            case 1:
                i = Random.Range(0, Enemies_One.Length);
                enemy = (Rigidbody)Instantiate(Enemies_One[i], SpawnPoints[aNum].transform.position, SpawnPoints[aNum].transform.rotation);
                break;
            case 2:
                i = Random.Range(0, Enemies_Two.Length);
                enemy = (Rigidbody)Instantiate(Enemies_Two[i], SpawnPoints[aNum].transform.position, SpawnPoints[aNum].transform.rotation);
                break;
            case 3:
                i = Random.Range(0, Enemies_Three.Length);
                enemy = (Rigidbody)Instantiate(Enemies_Three[i], SpawnPoints[aNum].transform.position, SpawnPoints[aNum].transform.rotation);
                break;
        }
    }
    // END: void SpawnNPC() ************************************************************

    //**********************************************************************************
    // void DespawnNPC()
    //
    // Destroys the spawned enemy
    // Called only when player loses, if the enemy is killed it will despawn itself
    //**********************************************************************************
    public void DespawnNPC()
    {
        Destroy(enemy.gameObject, 0);
        enemy = null;
    }
    // END: void DespawnNPC() **********************************************************

    //**********************************************************************************************************
    // PLAYER SPAWNING METHODS
    //**********************************************************************************************************
    //**********************************************************************************
    // void TeleportPlayer(int loc)
    //  int loc: Spawn location in the player spawn point array
    //
    // Teleports the player to the requested location
    //**********************************************************************************
    public void TeleportPlayer(int loc)
    {
        player.position = SpawnPoints_Player[loc].transform.position;
        player.rotation = SpawnPoints_Player[loc].transform.rotation;
    }
}

