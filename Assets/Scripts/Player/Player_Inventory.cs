﻿using UnityEngine;


public class Player_Inventory : MonoBehaviour {

    //Player information
    private Player_Manager pInfo;

    private GameObject[] inv;                    //Array containing our list of inventory items
    public ItemDetails[] iInfo;                 //Details about inventory items
    private int playerGold;                      //Amount of gold the player has
    [SerializeField]                            //Equipped Items
    private int[] playerEquipped;                //inventory location of Equipped items
                                                // 0 - Shield
                                                // 1 - Weapon
                                                // 2 - Armor
    

    //Stored weapon gameobjects for enabling/disabling display
    public GameObject[] equippedObject;

    void Start()
    {
        //Get player reference
        pInfo = GetComponent<Player_Manager>();

        //Starting a new game
        if (pInfo.IsNewGame)
        {
            // Set our starting vars
            inv = new GameObject[25];
            iInfo = new ItemDetails[25];

            playerGold = 50;                        //Set the players starting gold

            //Set all equip slots to nothing
            playerEquipped = new int[3] { -1, -1, -1 };

            //Equipped item rendering system
            for (int c = 0; c < equippedObject.Length; c++)
            {
                equippedObject[c].SetActive(false);
            }
            pInfo.gMan.UpdateInventoryPanel();
        }
        //Loading saved game
        else
        {

        }
    }

    //*************************************************************************************
    // public bool AddItem(GameObject obj)
    //  GameObject obj: item to add to the inventory
    //
    // Attempts to add the requested item to the first open slot in the player's inventory
    // Returns true if successful, false if not
    //*************************************************************************************
    public bool AddItem(GameObject obj)
    {
        // Adds an item to the players inventory
        // obj : GameObject = object to be added to inventory
        bool success = false;
        //Loop through our inventory to find the first open slot
        for (int i = 0; i < inv.Length; i++)
        {
            //Add the item to the inventory and give message
            if (inv[i] == null)
            {
                //Set the item slot to the new item
                inv[i] = obj;
                //Set the items information for use
                iInfo[i] = inv[i].GetComponent<ItemDetails>();

                //Update our inventory display
                pInfo.gMan.UpdateInventoryPanel();

                //Return successful
                success = true;
                return success;

            }
            //See if we have run out of room to place more items
            if (i == inv.Length - 1)
            {
                if (inv[i] != null)
                {
                    //Send message to GUI
                    pInfo.gMan.ShowMessage("Inventory Full.");
                    //Return unsuccessful
                    success = false;
                }
            }
        }
        return success;
    }
    // END: bool AddItem(GameObject obj) **************************************************

    //*************************************************************************************
    // public void RemoveItem(int invNum)
    //  int invNum: position in the inventory array to remove the item from
    //
    // Removes an item from the players inventory only.
    // What happens to the item must be handled by the calling method
    //*************************************************************************************
    public void RemoveItem(int invNum)
    {
        //Set the item slot to null
        inv[invNum] = null;
        //Set the items info to null
        iInfo[invNum] = null;
        //Update our inventory display
        pInfo.gMan.UpdateInventoryPanel();
    }
    // END: void RemoveItem(int invNum) ***************************************************

    //*************************************************************************************
    // public void EquipItem(int invNum)
    //  int invNum: inventory number of the item to equip
    // Equips an item from inventory 
    //*************************************************************************************
    public void EquipItem(int invNum)
    {

        
        //Unequip existing item in the slot
        if (playerEquipped[iInfo[invNum].ItemType] != -1)
        {
            UnEquipItem(playerEquipped[iInfo[invNum].ItemType]);
        }
       
        //Set the equipped item
        playerEquipped[iInfo[invNum].ItemType] = invNum;

        //Display the object in game
        pInfo.ActivateEquipped(iInfo[invNum].Name);

        //Set our mods
        for (int i = 0; i < pInfo.pStats.Mods.Length; i++)
        {
            if (iInfo[invNum].Stats[i] != 0)
                pInfo.pStats.ItemMods[i] += iInfo[invNum].Stats[i];
        }
        //Update the player stats
        pInfo.pStats.UpdatePlayerStats();
        //Enable equipped "E" display
        pInfo.gMan.DisplayEquipped(invNum);

        
    }
    // END: void EquipItem() ****************************************************************

    //***************************************************************************************
    // public void UnEquipItem(int invNum)
    //  int invNum: inventory number of item to unequip
    //
    // Unequips an item in inventory
    //***************************************************************************************
    public void UnEquipItem(int invNum)
    {
        if (iInfo[invNum] != null)
        {

            //Display the object in game
            pInfo.DeactivateEquipped(iInfo[invNum].Name);

            //Remove our mods
            for (int i = 0; i < pInfo.pStats.Mods.Length; i++)
            {
                if (iInfo[invNum].Stats[i] != 0)
                    pInfo.pStats.ItemMods[i] -= iInfo[invNum].Stats[i];
            }

            //Disabled equipped display
            pInfo.gMan.DisplayEquipped(invNum);
            //Update gui
            pInfo.pStats.UpdatePlayerStats();
            //Remove the equipped item
            playerEquipped[iInfo[invNum].ItemType] = -1;


        }
        else
            return;
    }
    // END: void UnEquipItem(int invNum) ****************************************************

    //***************************************************************************************
    // public void UseItem(int invNum)
    //  int invNum: inventory number of the item to use
    //
    // Uses an item from inventory
    //***************************************************************************************
    public void UseItem(int invNum)
    {
        //Make sure we have an item
        if (iInfo[invNum] != null)
        {
            //If its a potion
            if (iInfo[invNum].ItemType == 3)
            {
                //Add vitals given in the items details
                pInfo.pStats.ModifyVital(0, iInfo[invNum].Stats[0]);
                pInfo.pStats.ModifyVital(2, iInfo[invNum].Stats[1]);
                pInfo.pStats.ModifyVital(4, iInfo[invNum].Stats[2]);
                //Clear item from hotkey if necessary
                pInfo.gMan.HotkeyClearUsedItem(invNum);
                //Clear item from inventory
                RemoveItem(invNum);
            }
        }
        //Unusable item so return
        else
        {
            //Send message to message handler
            pInfo.gMan.ShowMessage("Item unable to be used.");
            //Return doing nothing
            return;
        }
    }
    // END: void UseItem(int invNum) ********************************************************


    //******************************************************************************************
    // PROPERTIES
    //******************************************************************************************
    // - inv[]
    public GameObject[] Inventory
    {
        get
        {
            return inv;
        }
    }
    // - playerEquipped[]
    public int[] Equipped
    {
        get
        {
            return playerEquipped;
        }
    }
    // - playerGold
    public int Gold
    {
        get
        {
            return playerGold;
        }
        set
        {
            playerGold = value;
        }
    }
}
