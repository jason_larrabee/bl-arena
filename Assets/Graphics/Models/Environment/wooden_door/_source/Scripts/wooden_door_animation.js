#pragma strict

private var player : GameObject;
private var doorSound : AudioSource;
private var isOpen : boolean;
private var openTimer : float;
private var maxRange;

function Start () {
	player = GameObject.FindGameObjectWithTag("Player");
	doorSound = GetComponent(AudioSource);
	isOpen = false;
	openTimer = 0;
	maxRange = 5;
}
	
function Update () {
	//Checks if the user has pressed R
	if (Input.GetKeyUp("r")){
		//Get the distance to the player
		var distance = Vector3.Distance(player.transform.position, this.transform.position); 
		//If in range, open the door
		if (distance <= 5 && !isOpen){
    		GetComponent.<Animation>().Play("out-open-slowly");
    		doorSound.Play();
    		isOpen = true;
    		//Set the timer
    		openTimer += Time.deltaTime;
    	}
	}
	//If the door is open, update the close timer
	if (isOpen){
		UpdateTimer();
	}
} 

function UpdateTimer(){
//Function to update the door timer when it is open, and then to close it when the timer is up
	openTimer += Time.deltaTime;
	//After 3 seconds, close the door
	if (openTimer >= 3){
		GetComponent.<Animation>().Play("out-close");
		isOpen = false;
		//Reset timer to 0
		openTimer = 0;
	}
}


 //   animation.Play("out-close");


//
//if (Input.GetKey("1"))
//    { 
//    animation.Play("out-snap-open");
//    }
//
//if (Input.GetKey("2"))
//    {
//    animation.Play("out-slam-shut");
//    }
//
//if (Input.GetKey("3"))
//    {
//    animation.Play("out-open-slowly");
//    } 
//
//if (Input.GetKey("4"))
//    {
//    animation.Play("out-close");
//    }
//
//if (Input.GetKey("5"))
//    {
//    animation.Play("in-snap-open");
//    }
//
//if (Input.GetKey("6"))
//    {
//    animation.Play("in-slam-shut");
//    }
//
//if (Input.GetKey("7"))
//    {
//    animation.Play("in-open-slowly");
//    }        
//
//if (Input.GetKey("8"))
//    {
//    animation.Play("in-close");
//    }

