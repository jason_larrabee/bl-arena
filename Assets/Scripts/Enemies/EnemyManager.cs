﻿using UnityEngine;
using System.Collections;

public class EnemyManager : MonoBehaviour {
    //*****************************************************************************************
    // EnemyManager.cs
    //
    // Script to tie all the enemy scripts into one package for easier referencing among scripts
    // Also contains "global enemy" variables
    // 
    // Attach to an enemy then set the references in the inspector
    //*****************************************************************************************

    //Player reference
    private Player_Manager pInfo;

    //Enemy scripts & references
    public EnemyStats eStats;
    public EnemySkills eSkills;
    public EnemyInventory eInv;
    public EnemyAnimation eAnim;
    public EnemyCombat eCom;
    public NavMeshAgent navAgent;
    public GUIManager gMan;

    [SerializeField]
    private string name;

    //Flags
    private bool isDead = false;

    void Awake()
    {
        pInfo = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Manager>();
    }

    //*****************************************************************************************
    // void DamageEnemy(int attackType, float maxDamage)
    //  int attackType: type of attack that hit
    //      0 - Melee   (Physical)
    //      1 - Ranged  (Physical)
    //      2 - Magic   (Magical)
    //  float maxDamage: maximum potential damage being passed by the player
    //
    // Determines the final amount of damage to take, takes the damage, and sends the information
    //  back to the player for experience calculations
    //*****************************************************************************************
    public void DamageEnemy(int attackType, float maxDamage)
    {
        //Determine actual damage
        float diff = 0.0f;
        if (attackType < 2)
            diff = maxDamage - eStats.PhysicalDefense;
        else if (attackType == 2)
            diff = maxDamage - eStats.MagicDefense;
        //Ensure the player always has a chance to do some damage on successful hits
        if (diff < 5.0f) diff = 5.0f;
        float damage = Random.Range(0.0f, diff);
        //Apply damage
        eStats.ModifyVital(0, -damage);
        //Send info to player
        pInfo.GiveCombatExperience(attackType, damage);
    }
    // END: void DamageEnemy(float maxDamage) *************************************************

    //*****************************************************************************************
    // void KillEnemy()
    //
    // Kills the enemy
    //*****************************************************************************************
    public void KillEnemy()
    {
        if (isDead)
        {
            //Let player manager know enemy is dead
            pInfo.EnemyDead = true;
            //Give player gold reward
            pInfo.pInv.Gold += eInv.Gold;
            //("Reward: " + eInv.Gold);

            //Clean up the dead body
            StartCoroutine(DestroyEnemy());
        }
    }
    // END: void KillEnemy() ******************************************************************

    //*****************************************************************************************
    // IEnumerator DestroyEnemy()
    //
    // Waits 5 seconds and destroys the game object
    //*****************************************************************************************
    IEnumerator DestroyEnemy()
    {
        yield return new WaitForSeconds(5.0f);
        Destroy(this.gameObject);
    }
    // END: IEnumerator DestroyEnemy() ********************************************************

    //*****************************************************************************************
    // PROPERTIES
    //*****************************************************************************************
    // bool eIsDead
    public bool IsDead
    {
        get
        {
            return isDead;
        }
        set
        {
            isDead = value;
        }
    }

    // string name
    public string Name
    {
        get
        {
            return name;
        }
    }

}
