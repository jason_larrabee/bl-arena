﻿using UnityEngine;


public class EnemyStats : MonoBehaviour {
    //***********************************************************************
    // EnamyStats.cs
    //
    // Holds and handles the enemy's stats
    //***********************************************************************
    // Reference to manager
    [SerializeField]
    private EnemyManager eInfo;

    // VITALS
    [SerializeField]
    public float[] stats_vitals;
    // - 0/1 - Health/Max
    // - 2/3 - Stamina/Max
    // - 4/5 - Mana/Max

    // PRIMARY STATS
    [SerializeField]
    private float[] stats_primary;
    // - 0 - Strength
    // - 1 - Dexterity
    // - 2 - Intelligence

    // SECONDARY STATS
    [SerializeField]
    private float[] stats_secondary;
    // - 0 - Melee Attack
    // - 1 - Ranged Attack
    // - 2 - Magic Attack
    // - 3 - Physical Defense
    // - 4 - Magical Defense

    // DERIVED STATS
    // These are the final computed versions of the secondary stats with all modifiers applied.  These are the stats accessed during combat checks and in the UI.
    [SerializeField]
    private float[] stats_derived;
    // - 0 – Max Health
    // - 1 – Max Stamina
    // - 2 – Max Mana
    // - 3 – Strength
    // - 4 – Dexterity
    // - 5 - Intelligence
    // - 6 - Melee Attack
    // - 7 - Ranged Attack
    // - 8 - Magic Attack
    // - 9 - Physical Defense
    // - 10 - Magical Defense 

    // EFFECT BONUS – This is the cumulative mods applied to each of the stats.  The bonuses are from equipment, spells, or skills.
    [SerializeField]
    private float[] totalMods;
    // - 0 – Max Health
    // - 1 – Max Stamina
    // - 2 – Max Mana
    // - 3 – Strength
    // - 4 – Dexterity
    // - 5 - Intelligence
    // - 6 - Melee Attack
    // - 7 - Ranged Attack
    // - 8 - Magic Attack
    // - 9 - Physical Defense
    // - 10 - Magical Defense 

    //**********************************************************************
    // void Start()
    //
    // Sets up the stats and determines starting mod values
    //**********************************************************************
    void Start()
    {
        //Calculate our derived attributes
        // Character base stats must be set in the inspector before this will work right, otherwise everything comes back 0
        CalculateDerivedStats();
    }
    // END: void Start() ***************************************************

    //******************************************************************************************
    // void CalculateDerivedStats()
    //
    // Calculates and sets derived stats based on primary  and secondary values
    //******************************************************************************************
    private void CalculateDerivedStats()
    {
        //Primary Stats
        stats_derived[3] = stats_primary[0] + totalMods[3];             // Strength
        stats_derived[4] = stats_primary[1] + totalMods[4];             // Dexterity
        stats_derived[5] = stats_primary[2] + totalMods[5];             // Intelligence
        // - Calculate vital mods from primary stats
        totalMods[0] = (stats_derived[3] / 5.0f);                       //MaxHealth Mod = Strength/5;
        totalMods[1] = (stats_derived[4] / 5.0f);                       //MaxStamina Mod = Dexterity/5;
        totalMods[2] = (stats_derived[5] / 5.0f);                       //MaxMana Mod = Intelligence/5;
        // - Calculate secondary mods from primary stats
        totalMods[6] = ((stats_derived[3] + stats_derived[4]) / 2.0f);  // Melee Attack = (Strength + Dexterity)/2
        totalMods[7] = ((stats_derived[3] + stats_derived[4]) / 2.0f);  // Ranged Attack = (Strength + Dexterity)/2
        totalMods[8] = (stats_derived[5]);                              // Magic Attack = Intelligence
        totalMods[9] = ((stats_derived[3] + stats_derived[4]) / 3.0f);  // Physical Defense = (Strength + Dexterity)/3
        totalMods[10] = ((2.0f * stats_derived[5]) / 3.0f);             // Magic Defense = (2 * Intelligence)/3

        //Vital Stats
        stats_derived[0] = stats_vitals[1] + totalMods[0];              // Max Health
        stats_derived[1] = stats_vitals[3] + totalMods[1];              // Max Stamina
        stats_derived[2] = stats_vitals[5] + totalMods[2];              // Max Mana

        //Secondary Stats
        stats_derived[6] = stats_secondary[0] + totalMods[6];           // Melee Attack
        stats_derived[7] = stats_secondary[1] + totalMods[7];           // Ranged Attack
        stats_derived[8] = stats_secondary[2] + totalMods[8];           // Magic Attack
        stats_derived[9] = stats_secondary[3] + totalMods[9];           // Physical Defense
        stats_derived[10] = stats_secondary[4] + totalMods[10];         // Magical Defense
    }
    // END: void CalculateDerivedStats() *******************************************************

    //******************************************************************************************
    // void ModifyVital(int vital, float amount)
    //  int vital: Vital stat to modify
    //  float amount: amount to modify by (+ remove damage, - apply damage)
    //
    // Modifies the vital stat by the amount passed
    // Used to apply or remove damage from the enemy
    //******************************************************************************************
    public void ModifyVital(int vital, float amount)
    {
        //Apply the modification
        stats_vitals[vital] += amount;

        //Limit to the derived maximum and 0
        if (stats_vitals[vital] > stats_derived[(vital / 2)])
        {
            stats_vitals[vital] = stats_derived[(vital / 2)];
        }
        if (stats_vitals[vital] <= 0.0f)
        {
            stats_vitals[vital] = 0.0f;
        }

        //Determine life/death status of the player
        if (stats_vitals[0] > 0.0f)
        {
            eInfo.IsDead = false;
        }
        else
        {
            eInfo.IsDead = true;
            eInfo.KillEnemy();
        }

    }


    //******************************************************************************************
    // PROPERTIES
    //******************************************************************************************
    public float Health
    {
        get
        {
            return stats_vitals[0];
        }
    }
    public float MaxHealth
    {
        get
        {
            return stats_derived[0];
        }
    }
    public float Stamina
    {
        get
        {
            return stats_vitals[2];
        }
    }
    public float MaxStamina
    {
        get
        {
            return stats_derived[1];
        }
    }
    public float Mana
    {
        get
        {
            return stats_vitals[4];
        }
    }
    public float MaxMana
    {
        get
        {
            return stats_derived[2];
        }
    }
    public float Strength
    {
        get
        {
            return stats_derived[3];
        }
    }
    public float Dexterity
    {
        get
        {
            return stats_derived[4];
        }
    }
    public float Intelligence
    {
        get
        {
            return stats_derived[5];
        }
    }
    public float MeleeAttack
    {
        get
        {
            return stats_derived[6];
        }
    }
    public float RangedAttack
    {
        get
        {
            return stats_derived[7];
        }
    }
    public float MagicAttack
    {
        get
        {
            return stats_derived[8];
        }
    }
    public float PhysicalDefense
    {
        get
        {
            return stats_derived[9];
        }
    }
    public float MagicDefense
    {
        get
        {
            return stats_derived[10];
        }
    }
    public float[] Mods
    {
        get
        {
            return totalMods;
        }
        set
        {
            totalMods = value;
        }
    }
}
