﻿using UnityEngine;

public class ArenaGuard : MonoBehaviour {
    //************************************************************************************************
    // ArenaGuard.cs
    //
    // The Arena Guard is responsible for allowing the player in and out of the arena.
    // Tracks which arena he is responsible for
    //************************************************************************************************

    [SerializeField]
    private Transform spawnPoint;               //Point to spawn player on use

    [SerializeField]
    private SpawnManager spawnManager;          //Reference to the spawn manager

    [SerializeField]
    private int arenaNumber;                    //Arena to guard

    [SerializeField]
    private bool isExit = false;                //Exit guard?

    public void TeleportPlayer()
    {
        if (isExit)
            SendToLobby();
        else
            SendToArena();
    }

    private void SendToArena()
    {
        //Find the player
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Transform playerPos = player.GetComponent<Transform>();
        Player_Manager pInfo = player.GetComponent<Player_Manager>();

        //Teleport them to the arena
        playerPos.position = spawnPoint.position;
        pInfo.EnterArena();

        //Spawn the opponent
        spawnManager.SpawnNPC(arenaNumber);
    }

    private void SendToLobby()
    {
        //Find the player
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Transform playerPos = player.GetComponent<Transform>();
        Player_Manager pInfo = player.GetComponent<Player_Manager>();

        //Teleport them to the lobby
        playerPos.position = spawnPoint.position;
        pInfo.ExitArena();
 
    }
}
